{
  "name": "ah-me.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 88,
  "traversal": [
    {
      "orientation": "straight",
      "link": "http://www.ah-me.com/most-recent/page%s.html",
      "start_index": "1",
      "end_index": "1"
    },
    {
      "orientation": "gay",
      "link": "http://www.ah-me.com/gay/most-recent/page%s.html",
      "start_index": "1",
      "end_index": "1"
    },
    {
      "orientation": "transsexual",
      "link": "http://www.ah-me.com/shemale/most-recent/page%s.html",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "30",
    "count": "10"
  },
  "videos": {
    "classifier": "div.moviec",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "div.thumb-wrap a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "div.thumb-wrap a",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "div.thumb-wrap a img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.thumb-wrap a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(.+)/\\d+.jpg",
              "replacement": "$1/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.thumb-wrap a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+/(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "hd",
        "classifier": "div.thumb-wrap img.icon-hd"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.time",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.video",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "p.firstp a",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://embeds.ah-me.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}