from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from openpyxl import load_workbook
import re, os
import time, openpyxl

chrome_driver_path = '/Users/testguru/Downloads/chromedriver 4'

list_dom=[]
list_id=[]
list_column= []
list_row= []
list_domain_keyword= []

driver = webdriver.Chrome(executable_path=("%s" % chrome_driver_path))
driver.maximize_window()

def main():
    driver.get('http://deployer.com/cj_installer/admin/login')
    driver.implicitly_wait(10)
    cj_login_input = driver.find_element_by_xpath('//*[@id="username"]')
    cj_login_input.send_keys('логин')
    cj_password_input = driver.find_element_by_xpath('//*[@id="password"]')
    cj_password_input.send_keys('пароль')
    cj_Press_Button = driver.find_element_by_xpath('/html/body/div/div[2]/form/div[3]/div[2]/button')
    cj_Press_Button.click()
    sites_button = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/a')
    sites_button.click()
    show_not_completed_sites = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/ul/li[2]/a')
    show_not_completed_sites.click()
    pagging = driver.find_elements_by_css_selector('#DataTables_Table_0_paginate > ul li a')

    filenames = os.listdir('/Users/testguru/Downloads')
    for f in filenames:
        if f.startswith('Copy of' ) and f.endswith('.xlsx'):
            p = '/Users/testguru/Downloads/'+f
    wb = load_workbook(p)
    sheet = wb['Лист1']
    sheet =wb.active
    for row in sheet['A1':'A40']:
        for cell in row:
            list_column.append(cell.value)
    for row in sheet['E1':'E40']:
        for cell in row:
            list_row.append(cell.value)
    for el in range(len(list_column)):
        list_domain_keyword.append(str(list_column[el])+" "+ str(list_row[el]))

    for page in pagging[1:-1]:
        collect_domain_name = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(2)')
        collect_id_number = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(1)')
        for dom in collect_domain_name:
            list_dom.append(dom.text)
        for num in collect_id_number:
            list_id.append(num.text)
        page_button = driver.find_element_by_css_selector('#DataTables_Table_0_next > a')
        page_button.click()
        time.sleep(2)
    for site in list_dom:
        if site in list_column:
            index = list_dom.index(site)
            column_index = list_column.index(site)
            print(site + ' ID - ' + list_id[index] + ' KEYWORDS - ' + str(list_row[column_index]))
        #         if str(list_column[column_index]) +' '+str(list_row[column_index]) or str(list_column[column_index]) + " None" in list_domain_keyword:
            a = r'http://deployer.com/cj_installer/admin/sites/'+ list_id[index] + r'/edit'
            driver.get(a)
            private_sett_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/form/div[1]/ul/li[2]/a')
            private_sett_button.click()
            search_base_name = driver.find_element_by_xpath('//*[@id="search_base_name"]')
            if str(list_row[column_index]) == 'None':
                search_base_name.clear()
                save_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/form/div[2]/input')
                save_button.click()
            else:
                search_base_name.clear()
                search_base_name.send_keys(str(list_row[column_index]))
                save_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/form/div[2]/input')
                save_button.click()
    print(list_dom)
    print(list_id)
    driver.quit()
    os.remove(p)
main()