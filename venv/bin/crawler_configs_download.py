from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
import time

chrome_driver_path = '/Users/testguru/Downloads/chromedriver 4'
username = 'dfszf'
password = 'zsfdsdf'
base_url = 'localhost:8080/'  # ie 127.0.0.1:8080,
baseUrl = 'http://%s:%s@%s' % (username, password, base_url)
driver = webdriver.Chrome(executable_path=("%s" % chrome_driver_path))
driver.get(baseUrl)
driver.maximize_window()


path_for_config_downloading = '/Users/testguru/Desktop/crawler_configs_download'
path_for_dump_downloading = '/Users/testguru/Desktop/crawler_dump_download'
id_buttons = driver.find_elements_by_css_selector('#tubesTable > tbody:nth-child(2) > tr > td:nth-child(1) > a')

if not os.path.exists(path_for_config_downloading):
    os.mkdir(path_for_config_downloading)
if not os.path.exists(path_for_dump_downloading):
    os.mkdir(path_for_dump_downloading)

def config_dumps_text():
    for i in range(len(id_buttons)):
        a = driver.find_elements_by_css_selector('#tubesTable > tbody tr td:nth-child(1) a')[i]
        a.click()
        scrolling_button = driver.find_element_by_xpath('//*[@id="config"]/div/div[1]/div/button')
        scrolling_button.click()
        code_text = driver.find_element_by_xpath('//*[@id="config"]/div/div[1]/div/div/div/ul/li[4]/button')
        code_text.click()
        configsText = driver.find_element_by_css_selector('.jsoneditor-text').get_attribute('value')
        file_name = driver.find_element_by_css_selector('input#name.form-control').get_attribute('value')
        crawl_name = path_for_config_downloading + '/' + file_name
        if not os.path.isfile(crawl_name):
            with open(crawl_name, 'w') as crawl_conf:
                crawl_conf.write(configsText)
        dumps_scrolling_button = driver.find_element_by_xpath('//*[@id="dump"]/div/div[1]/div/button')
        dumps_scrolling_button.click()
        dumps_code_text = driver.find_element_by_xpath('//*[@id="dump"]/div/div[1]/div/div/div/ul/li[4]/button')
        dumps_code_text.click()
        dumps_configs_field = driver.find_element_by_xpath('//*[@id="dump"]/div/div[2]/textarea').get_attribute('value')
        dump_name = path_for_dump_downloading + '/' + file_name
        if not os.path.isfile(dump_name):
            with open(dump_name, 'w') as dump_conf:
                dump_conf.write(dumps_configs_field)
        back_to_crawlers_button = driver.find_element_by_xpath('/html/body/div[2]/nav/div/div/a')
        back_to_crawlers_button.click()

config_dumps_text()
driver.quit()