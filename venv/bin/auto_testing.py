from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
import time, re
import requests
from requests.auth import HTTPBasicAuth
import pymysql.cursors
import random
from selenium.webdriver.common.keys import Keys
from webdriver_manager.firefox import GeckoDriverManager
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

proxies = {'http': 'прокси',
           'https': 'прокси'}

rand_it = random.randint(100, 9999999)

insert_list1 = []
list_for_test_link = []
list_for_test_status = []

username = 'username'
password = 'password'
base_url = 'password:8080/'  # ie 127.0.0.1:8080,
baseUrl = 'http://%s:%s@%s' % (username, password, base_url)

connection = pymysql.connect(host='localhost',
                             user='user',
                             password='password',
                             db='89pe',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor) #cursors.DictCursor

def count_rows():
    global count_video, count_thumb_reviewed
    with connection.cursor() as cursor:
        sql = 'SELECT COUNT(*) FROM video'
        cursor.execute(sql)
        count_video = cursor.fetchall()
    connection.commit()
    with connection.cursor() as cursor:
        sql = 'SELECT COUNT(*) FROM thumb_reviewed'
        cursor.execute(sql)
        count_thumb_reviewed = cursor.fetchall()
    connection.commit()

def input_data():
    global driver
    inp_num = input("Введите номер Tube ")
    inp_selector = input("Введите селектор ")
    inp_val = input("Введите value ")

    driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
    driver.get(baseUrl)
    driver.maximize_window()
    link = 'link' + str(inp_num)
    driver.get(link)
    time.sleep(1)
    dumps_scrolling_button = driver.find_element_by_xpath('//*[@id="dump"]/div/div[1]/div/button')
    dumps_scrolling_button.click()
    dumps_code_text = driver.find_element_by_xpath('//*[@id="dump"]/div/div[1]/div/div/div/ul/li[4]/button')
    dumps_code_text.click()
    dumps_configs_field = driver.find_element_by_xpath('//*[@id="dump"]/div/div[2]/textarea')
    # print(dumps_configs_field.get_attribute('value'))
    # strk_sel_val = r'\"deletion\".+\n.+\n.+\"selector\"\:\s\"(.+)\"\,\n.+\"value\":\s\"(.+)\"'
    strk_sel_val = r'\"deletion\".+\n.+\n.+\"selector\"\:\s\"((.+)|())\"\,\n.+\"value\":\s\"((.+)|())\"'

    finder_sel = re.findall(strk_sel_val , dumps_configs_field.get_attribute('value'))
    print(finder_sel)
    finder_sel.clear()

    t = re.sub(strk_sel_val, r""""deletion": {
    "type": "CONTENT",
    "selector": """ + '"' + inp_selector +'"' + """,
    "value": """ + '"' + inp_val +'"' + """""", dumps_configs_field.get_attribute('value'))

    time.sleep(2)
    dumps_configs_field.clear()
    dumps_configs_field.send_keys(t)
    print(t)
    submit_button = driver.find_element_by_xpath('/html/body/div[2]/div/form/div[6]/button')
    submit_button.click()
    time.sleep(4)
    driver.close()

def cleanup():
    print('Отправить Post запрос на http://localhost:8080/jobs/cleanupThumbReviewedVideos ? y / n')
    answ = input()
    if answ == 'y':
        url = r'http://localhost:8080/jobs/cleanupThumbReviewedVideos'
        response = requests.post(url, auth=HTTPBasicAuth('логин', 'пароль'))
        with connection.cursor() as cursor:
            sql = 'Select link From video Where deleted_date is null'
            cursor.execute(sql)
            result = cursor.fetchall()
        connection.commit()
    elif answ == 'n':
        print('Заппрос не отправлен')
    else:
        pass
    print('CleanupThumbReviewedVideos выполнена, Прошло 5 сек ? y / n')
    answ_del = input()
    if answ_del == 'y':
        pass


def mysql_inserts():

    ##Сделать переменную input(---обязателен ID, сделать random[100 - 999999]), которую потом добавить в список через append
    input_link = input('Введите url для проверки' + '\n')
    input_tube_num =input('Введите номер туба' + '\n')
    row_for_video_ins = r"""INSERT INTO `video` (`id`,`categories`,`created_date`,`description`,`duration`,`hd`,`images_processed`,`link`,`models`,`orientation`,`site_id`,`tags`,`thumb`,`title`,`tube_id`,`source`,`thumb_count`,`paysite_id`,`deleted_date`,`last_update_date`,`category_processed`,`thumb_path`,`site_created_date`,`ctr`,`md5`,`main_thumb_number`,`vr`,`professional`,`preview_downloaded`,`downloaded`,`video_preview`) VALUES (""" + str(rand_it) + """,'Hardcore;German;Vintage',1484441757445,NULL,'PT5483S',b'0',b'1','""" + str(input_link) + """',NULL,0,'""" + str(rand_it) + """','','https://thumb-v-ec.xhcdn.com/t/809/%s_b_7106809.jpg','Casting Villa',""" + str(input_tube_num) + """,0,10,NULL,NULL,1564962462837,b'1','/%s/000/000/008',NULL,NULL,'b1d8b3ae9ceb3d7c577f6d487d8e3a06',NULL,b'0',b'0',0,b'0',NULL);"""
    row_for_thumb_ins = r"""INSERT INTO `thumb_reviewed` (`id`,`video_id`,`img_num`,`reviewed_date`,`user_id`,`count`,`reviewed`,`segment`,`predict_score`,`cropped`,`preview_downloaded`) VALUES (""" + str(rand_it) + """,""" + str(rand_it) + """,1,'2020-01-17 10:08:02',1,0,'[]',1,NULL,1,0);"""

    insert_list = [r"""INSERT INTO `video` (`id`,`categories`,`created_date`,`description`,`duration`,`hd`,`images_processed`,`link`,`models`,`orientation`,`site_id`,`tags`,`thumb`,`title`,`tube_id`,`source`,`thumb_count`,`paysite_id`,`deleted_date`,`last_update_date`,`category_processed`,`thumb_path`,`site_created_date`,`ctr`,`md5`,`main_thumb_number`,`vr`,`professional`,`preview_downloaded`,`downloaded`,`video_preview`) VALUES (3,'Amateur;18 Years Old;Teens;Facials;Deutsche;German;Treff Deine Nachbarin',1484441756071,NULL,'PT468S',b'0',b'1','https://xhamster.com/videos/deutsche-teen-nachbarin-im-schwimmbad-gefickt-7107256',NULL,0,'7107256','','https://thumb-v-ec.xhcdn.com/t/256/%s_b_7107256.jpg','Deutsche Teen Nachbarin im Schwimmbad gefickt',3,0,10,NULL,NULL,1564962462839,b'1','/%s/000/000/003',NULL,NULL,'ca82bb1a82d4c7c12ed3e53bca5f1a31',NULL,b'0',b'0',0,b'0',NULL);""",
r"""INSERT INTO `video` (`id`,`categories`,`created_date`,`description`,`duration`,`hd`,`images_processed`,`link`,`models`,`orientation`,`site_id`,`tags`,`thumb`,`title`,`tube_id`,`source`,`thumb_count`,`paysite_id`,`deleted_date`,`last_update_date`,`category_processed`,`thumb_path`,`site_created_date`,`ctr`,`md5`,`main_thumb_number`,`vr`,`professional`,`preview_downloaded`,`downloaded`,`video_preview`) VALUES (8,'Hardcore;German;Vintage',1484441757445,NULL,'PT5483S',b'0',b'1','https://xhamster.com/videos/bitches-with-bushes-13232765',NULL,0,'7106809','','https://thumb-v-ec.xhcdn.com/t/809/%s_b_7106809.jpg','Casting Villa',3,0,10,NULL,NULL,1564962462837,b'1','/%s/000/000/008',NULL,NULL,'b1d8b3ae9ceb3d7c577f6d487d8e3a06',NULL,b'0',b'0',0,b'0',NULL);""",
r"""INSERT INTO `video` (`id`,`categories`,`created_date`,`description`,`duration`,`hd`,`images_processed`,`link`,`models`,`orientation`,`site_id`,`tags`,`thumb`,`title`,`tube_id`,`source`,`thumb_count`,`paysite_id`,`deleted_date`,`last_update_date`,`category_processed`,`thumb_path`,`site_created_date`,`ctr`,`md5`,`main_thumb_number`,`vr`,`professional`,`preview_downloaded`,`downloaded`,`video_preview`) VALUES (6527613,'Hardcore;Pornstar;Masturbation;HD;Babe;Striptease',1497966584292,NULL,'PT1453S',b'1',b'1','https://www.pornhub.com/view_video.php?viewkey=ph55a4f1e58593c','Tiffany Thompson',0,'ph55a4f1e58593c','ass;tits;young;hottie;masturbate;solo;striptease;tease;vibrator;pussy;pool table;brunette;natural tits;teenager','https://ci.phncdn.com/videos/201507/14/52765141/original/%s.jpg','Hot player - Tiffany Thompson',40,0,10,NULL,NULL,1564966429440,b'1','/%s/006/527/613',NULL,NULL,'5b2f9c8c620a35a6049fd061a7a65d29',NULL,b'0',NULL,0,b'0',NULL);""",
r"""INSERT INTO `video` (`id`,`categories`,`created_date`,`description`,`duration`,`hd`,`images_processed`,`link`,`models`,`orientation`,`site_id`,`tags`,`thumb`,`title`,`tube_id`,`source`,`thumb_count`,`paysite_id`,`deleted_date`,`last_update_date`,`category_processed`,`thumb_path`,`site_created_date`,`ctr`,`md5`,`main_thumb_number`,`vr`,`professional`,`preview_downloaded`,`downloaded`,`video_preview`) VALUES (9812909,'MILFs;Russian;Wife;Funny;Escort',1513938310775,'I\'m a whore from the city of Labytnangi','PT55S',b'0',b'1','https://xhamster.com/videos/oksana-demenko-8727498a',NULL,0,'8727498','','https://thumb-v-ec.xhcdn.com/t/498/%s_b_8727498.jpg','Oksana Demenko',3,0,10,NULL,NULL,1564970228472,b'1','/%s/009/812/909',NULL,NULL,'b1b046ccefa4ab3cb38147c0a161fe08',NULL,b'0',NULL,0,b'0',NULL);""",
r"""INSERT INTO `thumb_reviewed` (`id`,`video_id`,`img_num`,`reviewed_date`,`user_id`,`count`,`reviewed`,`segment`,`predict_score`,`cropped`,`preview_downloaded`) VALUES (3,3,1,'2020-01-17 10:08:02',1,0,'[]',1,NULL,1,0);""",
r"""INSERT INTO `thumb_reviewed` (`id`,`video_id`,`img_num`,`reviewed_date`,`user_id`,`count`,`reviewed`,`segment`,`predict_score`,`cropped`,`preview_downloaded`) VALUES (10,6527613,1,'2020-01-16 14:30:26',1,0,'[]',1,NULL,1,0);""",
r"""INSERT INTO `thumb_reviewed` (`id`,`video_id`,`img_num`,`reviewed_date`,`user_id`,`count`,`reviewed`,`segment`,`predict_score`,`cropped`,`preview_downloaded`) VALUES (11,9812909,1,'2020-01-16 14:33:12',1,0,'[]',1,NULL,1,0);""",
r"""INSERT INTO `thumb_reviewed` (`id`,`video_id`,`img_num`,`reviewed_date`,`user_id`,`count`,`reviewed`,`segment`,`predict_score`,`cropped`,`preview_downloaded`) VALUES (8,8,1,'2020-01-21 16:50:28',1,0,'[]',1,NULL,1,0);""",
r"""INSERT INTO `video` (`id`,`categories`,`created_date`,`description`,`duration`,`hd`,`images_processed`,`link`,`models`,`orientation`,`site_id`,`tags`,`thumb`,`title`,`tube_id`,`source`,`thumb_count`,`paysite_id`,`deleted_date`,`last_update_date`,`category_processed`,`thumb_path`,`site_created_date`,`ctr`,`md5`,`main_thumb_number`,`vr`,`professional`,`preview_downloaded`,`downloaded`,`video_preview`) VALUES (5,'MILFs;Russian;Wife;Funny;Escort',1513938310775,'I\'m a whore from the city of Labytnangi','PT55S',b'0',b'1','https://xhamster.com/videos/kai-nobel-collection-2-and-erica-piri-4865665',NULL,0,'872749','','https://thumb-v-ec.xhcdn.com/t/498/%s_b_8727498.jpg','Oksana Demenko',3,0,10,NULL,NULL,1564970228472,b'1','/%s/009/812/909',NULL,NULL,'b1b046ccefa4ab3cb38147c0a161fe08',NULL,b'0',NULL,0,b'0',NULL);""",
r"""INSERT INTO `thumb_reviewed` (`id`,`video_id`,`img_num`,`reviewed_date`,`user_id`,`count`,`reviewed`,`segment`,`predict_score`,`cropped`,`preview_downloaded`) VALUES (5,5,1,'2020-01-17 10:08:02',1,0,'[]',1,NULL,1,0);"""]

    insert_list1.append(row_for_video_ins)
    insert_list1.append(row_for_thumb_ins)
    # print(insert_list1)
    # print(row_for_video_ins)
    # print(row_for_thumb_ins)

    print('Добавить записи в базы? y / n')
    print('Нажать ввод, чтобы показать Links From video Where deleted_date is null')
    answ_ins = input()
    if answ_ins == 'y':
        try:
            for ins in insert_list1:
                sql = ins
                with connection.cursor() as cursor:
                    cursor.execute(sql)
                    result = cursor.fetchall()
                connection.commit()
            with connection.cursor() as cursor:
                sql = 'Select link From video Where deleted_date is null'
                cursor.execute(sql)
                result = cursor.fetchall()
                # print(result)
                # for res in result:
                #     print(res)
            connection.commit()
        except:
            pass
    elif answ_ins == 'n':
        print('Записи не добавлены')
        with connection.cursor() as cursor:
            sql = 'Select link From video Where deleted_date is null'
            cursor.execute(sql)
            result = cursor.fetchall()
            for res in result:
                print(res)
        connection.commit()
    else:
        with connection.cursor() as cursor:
            sql = 'Select link From video Where deleted_date is null'
            cursor.execute(sql)
            result = cursor.fetchall()
            print('links From video Where deleted_date is null')
            for res in result:
                print(res)
        connection.commit()
    count_rows()

    # assert len(insert_list1) == (int(count_video[0]['COUNT(*)']) + int(count_thumb_reviewed[0]['COUNT(*)'])), 'Количество Inserts не соответствует Количеству записей'
    insert_list1.clear()

def mysql_del():

    deleted_list = [r"""DELETE FROM `89pe`.`video` WHERE (`id` = '6527613');""",
r"""DELETE FROM `89pe`.`thumb_reviewed` WHERE (`id` = '10');""",
r"""DELETE FROM `89pe`.`video` WHERE (`id` = '9812909');""",
r"""DELETE FROM `89pe`.`thumb_reviewed` WHERE (`id` = '11');""",
r"""DELETE FROM `89pe`.`video` WHERE (`id` = '3');""",
r"""DELETE FROM `89pe`.`thumb_reviewed` WHERE (`id` = '3');""",
r"""DELETE FROM `89pe`.`video` WHERE (`id` = '5');""",
r"""DELETE FROM `89pe`.`thumb_reviewed` WHERE  (`id` = '5');""",
r"""DELETE FROM `89pe`.`video` WHERE (`id` = '8');""",
r"""DELETE FROM `89pe`.`thumb_reviewed` WHERE  (`id` = '8');"""]

    print('Удалить записи из базы? y / n')
    # print('Нажать ввод, чтобы показать Links From video Where deleted_date is null')
    answ_del = input()
    if answ_del == 'y':
        try:
            for dl in deleted_list:
                sql = dl
                with connection.cursor() as cursor:
                    cursor.execute(sql)
                    result = cursor.fetchall()
                connection.commit()
                count_rows()
                assert len(deleted_list) != (int(count_video[0]['COUNT(*)']) + int(count_thumb_reviewed[0]['COUNT(*)'])), 'Количество Deleted_Rows не соответствует Количеству записей'
        except:
            pass
    elif answ_del == 'n':
        print('Записи не удалены')
        count_rows()
        assert len(deleted_list) == (int(count_video[0]['COUNT(*)']) + int(count_thumb_reviewed[0]['COUNT(*)'])), 'Количество Deleted_Rows соответствует Количеству записей'
        # with connection.cursor() as cursor:
        #     sql = 'Select link From video Where deleted_date is null'
        #     cursor.execute(sql)
        #     result = cursor.fetchall()
        #     for res in result:
        #         print(res)
        # connection.commit()
    else:
        pass



def mysql_select():
        print('Select links From video Where deleted_date is not null')
        with connection.cursor() as cursor:
                sql = 'Select link, deleted_date From video Where deleted_date is not null'
                cursor.execute(sql)
                result = cursor.fetchall()
                for res in result:
                    # print(res)
                    r = requests.get(res['link'])
                    # print(r.status_code)
                    if res['deleted_date'] != None and str((r.status_code)).startswith('4'):
                        print(res['link'] + ' | ' + str(r.status_code) + ' | ' + str(res['deleted_date']) + ' - Passed')
                        assert res['deleted_date'] != None, res['link'] + ' | ' + str(r.status_code) + ' | ' + str(res['deleted_date']) + ' - Passed'
                    elif res['deleted_date'] != None and str((r.status_code)).startswith('2'):
                        print(res['link'] + ' | ' + str(r.status_code) + ' | ' + str(res['deleted_date']) + ' - Passed')
                        assert res['deleted_date'] != None, res['link'] + ' | ' + str(r.status_code) + ' | ' + str(res['deleted_date']) + ' - Passed'
                    else:
                        print(res['link'] + ' | ' + str(r.status_code) + ' | ' + str(res['deleted_date']) + ' - Failed')
                    # assert res[1]['deleted_date'] == 'None', 'link, Where deleted_date is Null'
                    assert res['deleted_date'] != None
        connection.commit()

        print('')
        print('Select links From video Where deleted_date is null')
        with connection.cursor() as cursor:
                sql = 'Select link, deleted_date From video Where deleted_date is null'
                cursor.execute(sql)
                result = cursor.fetchall()
                for res in result:
                    r = requests.get(res['link'],proxies=proxies)
                    if res['deleted_date'] == None and str((r.status_code)).startswith('2'):
                        print(res['link'] + ' | ' + str(r.status_code) + ' | ' + str(res['deleted_date']))
                    else:
                        print(res['link'] + ' | ' + str(r.status_code) + ' | ' + str(res['deleted_date']) + ' - Failed')
                    # assert res[1]['deleted_date'] == 'None', 'link, Where deleted_date is Null'
                    assert res['deleted_date'] == None
                    if r.status_code == 200:
                        list_for_test_link.append(res['link'])
                        list_for_test_status.append(r.status_code)
        connection.commit()


def video_check_ff():
    from seleniumwire import webdriver
    options = {
        'proxy': {
            'http': 'прокси',
            'https': 'прокси',
            'no_proxy': 'localhost,127.0.0.1'
            # excludes
        }   #'javascript.enabled': False
    }
    # Выключение Javascript
    profile = webdriver.FirefoxProfile()
    profile.DEFAULT_PREFERENCES['frozen']['javascript.enabled'] = False
    profile.set_preference("app.update.auto", False)
    profile.set_preference("app.update.enabled", False)
    profile.update_preferences()

    ff_driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(), seleniumwire_options=options, firefox_profile=profile)
    ff_driver.set_page_load_timeout(30)
    if len(list_for_test_link) != 0:
        for link in list_for_test_link:
            i = list_for_test_link.index(link)
            try:
                ff_driver.get(link)
                time.sleep(3)
                ff_d = ff_driver.find_elements_by_css_selector("div[id*='play']")
                #Наличие плеера на странице
                # for f in ff_d:
                #     print(f.get_attribute('id'))
                if len(ff_d) != 0:
                    print('Pass - Video,from - ' + link + ' is Playing')
                    row = 'Pass - Video,from - ' + link + ' is Playing'
                    assert 'is Playing' in row
                else:
                    frame = ff_driver.find_element_by_xpath('//iframe[@scrolling="no"]')
                    # ga = frame.get_attribute('src')
                    ff_driver.switch_to.frame(frame)
                    d = ff_driver.find_elements_by_css_selector('[id*="play"]')
                    if len(d) != 0:
                        print('PASS - Video is Playing in Iframe, from - ' + link)
                        assert list_for_test_status[i] == 200, 'PASS - Video is Playing in Iframe, from - ' + link
                    else:
                        print('Video,from - ' + link + ' was deleted')
                        assert list_for_test_status[i] != 200, 'FAILURE - Video,from - ' + link + ' was deleted, but deleted_date is null'
            except TimeoutException:
                print('FAILURE - Page ' + link + ' is not loading full, Need Check Manually')
                # pass
    ff_driver.quit()


def test_deleted_date():
    mysql_inserts()
    cleanup()
    mysql_select()
    # video_check_ff()
    # video_check_ff()


if __name__ == '__main__':
    # input_data()
    # test_mysql_inserts()
    # cleanup()
    # mysql_del()
    # mysql_select()
    test_deleted_date()
    connection.close()
    list_for_test_link.clear()
    list_for_test_status.clear()

