from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import os, re
import urllib.request


chrome_options = Options()
chrome_options.add_argument("--headless")

list_thumbs = []
list_keywords = []


path_for_yandex_images = r'path_for_yandex_images' # Путь для сохранения картинок
if not os.path.exists(path_for_yandex_images):
    os.mkdir(path_for_yandex_images)
print('Путь, где сохраняются картинки - ' + os.path.abspath(path_for_yandex_images))


def image_downloading_yandex():
    global driver
    keyword = input('\n' + 'Введите кейворды поиска' + '\n')
    finder = re.findall(r'(.+?),|(.+?$)', keyword)
    for s in finder:
        if s[0] != '':
            list_keywords.append(s[0].lstrip().rstrip())
        else:
            list_keywords.append(s[1].lstrip().rstrip())
    display = input('Отображать браузер? y/n' + '\n')
    if display == 'y':
        driver = webdriver.Chrome(ChromeDriverManager().install())
    else:
        driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)


def main():
    driver.maximize_window()
    driver.get('https://yandex.ru/images/')
    for el in list_keywords:
        search_bar = driver.find_elements_by_css_selector("input.input__control")[0]
        search_bar.clear()
        search_bar.send_keys(el + Keys.ENTER)
        time.sleep(2)
        end_page = driver.find_element_by_css_selector('a.competitors__link:nth-child(1)')
        while end_page.text != 'Google':
            driver.execute_script('window.scrollTo(0,1000000000)')
            time.sleep(1)
        thumbs = driver.find_elements_by_css_selector('img.serp-item__thumb.justifier__thumb')
        main_keyword = re.sub(' ', '_', list_keywords[0])
        keyword_real = re.sub(' ', '_', el)
        main_path = path_for_yandex_images + '/' + main_keyword
        path = main_path + '/' + keyword_real + '_'
        if not os.path.exists(main_path):
            os.mkdir(main_path)
        for thumb in thumbs:
            res = thumb.get_attribute('src')
            list_thumbs.append(res)
        for l in list_thumbs:
            a = list_thumbs.index(l)
            urllib.request.urlretrieve(l, path + str(a) + '.jpg')
        thumbs.clear()
        list_thumbs.clear()
    list_keywords.clear()

    driver.quit()


if __name__ == '__main__':
    image_downloading_yandex()
    main()
