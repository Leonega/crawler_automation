import requests
from requests.auth import HTTPBasicAuth

# list_k = []
# list_v = []

# Отправка Post запроса на http://localhost:8080/jobs/cleanupThumbReviewedVideos
url = r'http://localhost:8080/jobs/cleanupThumbReviewedVideos'
response = requests.post(url,  auth=HTTPBasicAuth('логин', 'пароль') )
# Статус код
st = response.status_code
print(str(st)+ '\n')

r = response.headers

# Содержимое headers
for k,v in r.items():
    # list_k.append(k)
    # list_v.append(v)
    print(k + '                    ' + v)
