{
  "name": "beeg.com",
  "botCheckMs": "0",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "https://beeg.com/api/v6/%s/index/main/%s/pc",
      "details_link": "https://beeg.com/api/v6/%s/video/%s",
      "end_index": "48",
      "traversal_type": "BEEG"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "36000"
  },
  "thumbs": {
    "start": "1",
    "end": "1",
    "count": "1"
  }
}