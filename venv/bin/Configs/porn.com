{
  "name": "porn.com",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 88,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.porn.com/videos?p=%s",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "5"
  },
  "videos": {
    "classifier": "div.main section.thumb-list div.thumb",
    "info": [
      {
        "videoInfoType": "title",
        "classifier": "a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+/sc/\\d+/\\d+/(\\d+)/promo/crop/.+",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "/sc/(\\d+/\\d+/\\d+/promo/crop)/\\d+/promo_(\\d+).jpg",
              "replacement": "/sc/$1/368/promo_%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+/sc/\\d+/\\d+/\\d+/promo/crop/\\d+/promo_(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.hd"
      },
      {
        "videoInfoType": "vr",
        "classifier": "span.vr span",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "section.video",
    "info": [
      {
        "videoInfoType": "runtime",
        "classifier": "div.header.flex span.length",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.meta-tags p:nth-child(2) span > a",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.meta-tags p:nth-child(1) span:nth-child(2) a:nth-child(1)",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.meta-tags p:nth-child(3) span > a",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.header.flex div.pornstars span > a",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.porn.com/videos/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}