{
  "name": "pornerbros.com",
  "botCheckMs": "1",
  "stopNumberExistingVideosInRow": 40,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.pornerbros.com/videos?p=%s&sort=date",
      "start_index": "1",
      "end_index": "1"
    },
    {
      "orientation": "gay",
      "link": "https://www.pornerbros.com/gay/videos?p=%s&sort=date",
      "start_index": "1",
      "end_index": "1"
    },
    {
      "orientation": "transsexual",
      "link": "https://www.pornerbros.com/shemale/videos?p=%s&sort=date",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "40",
    "count": "10"
  },
  "videos": {
    "classifier": " div.video-item ",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?_(\\d+)",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": " a img",
        "place": "attr",
        "place_name": "abs:src"
      },
      {
        "videoInfoType": "hd",
        "classifier": "li.topHD",
        "place": "text"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "li.duration-top",
        "place": "text"
      },
      {
        "videoInfoType": "title",
        "classifier": "a img",
        "place": "attr",
        "place_name": "alt"
      }
    ]
  },
  "video_details": {
    "classifier": "div.video-description.row",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.more-content div.labels a.btn.btn-secondary",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "span.pornstars span a ",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "multiple": "true",
        "classifier": "div.more-content div.channel-img a",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.pornerbros.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}