{
  "name": "youporn.com",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.youporn.com/browse/time/?page=%s",
      "start_index": "1",
      "end_index": "1",
      "cookies": {
        "age_verified": "1"
      }
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "2",
    "end": "16",
    "count": "10"
  },
  "videos": {
    "classifier": "div.video-box.four-column.video_block_wrapper",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a.video-box-image",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "data-video-id"
      },
      {
        "videoInfoType": "title",
        "classifier": "a.video-box-image img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a.video-box-image img",
        "place": "attr",
        "place_name": "data-thumbnail",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(https://.+?)/(\\d+/\\d+/\\d+)/.+?/\\d+/.+?.jpg",
              "replacement": "$1/$2/original/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a.video-box-image img",
        "place": "attr",
        "place_name": "data-thumbnail",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?/\\d+/\\d+/\\d+/.+?/(\\d+)/.+?.jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.video-duration",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "i.icon.icon-hd-text"
      },
      {
        "videoInfoType": "vr",
        "classifier": "i.icon.icon-vr-text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.main_content",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.categoriesWrapper.js_categoriesWrapper a[href^='/category/']",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.categoriesWrapper.js_categoriesWrapper a[href^='/pornstar/']",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "classifier": "div.underplayerInfoBox div#description",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.tagBoxContent a",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.submitByLink a[href^='/channel/']",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.youporn.com/embed/%s/\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}