{
  "name": "tubepornclassic",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "https://tubepornclassic.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "10"
  },
  "videos": {
    "classifier": "div#list_videos_latest_videos_list_items.margin-fix div.item a",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.img img",
        "place": "attr",
        "place_name": "abs:src"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.thumb__hd",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#tab_video_info ",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.info div.item:nth-child(3)> a",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.info div.item:nth-child(4)> a",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.info div.item:nth-child(5)> a",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.info div.item em",
        "place": "text"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.info div.item span:nth-child(2)> em",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://tubepornclassic.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}