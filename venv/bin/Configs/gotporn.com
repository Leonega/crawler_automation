{
  "name": "gotporn.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 50,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.gotporn.com/?page=%s",
      "start_index": "1",
      "end_index": "1"
    },
    {
      "orientation": "gay",
      "link": "https://www.gotporn.com/gay?page=%s",
      "start_index": "1",
      "end_index": "1"
    },
    {
      "orientation": "transsexual",
      "link": "https://www.gotporn.com/shemale?page=%s",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "30",
    "count": "10"
  },
  "videos": {
    "classifier": "ul.video-items-feed.clearfix li.video-item",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "data-id"
      },
      {
        "videoInfoType": "title",
        "classifier": "a",
        "place": "attr",
        "place_name": "data-title"
      },
      {
        "videoInfoType": "hd",
        "classifier": "a h3.video-thumb-title.hd ",
        "place": "text"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a span.video-thumb span.video-thumb img.video-thumb-img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "\\d+.240.180.jpg",
              "replacement": "%s.orig.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a span.video-thumb span.video-thumb img.video-thumb-img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?/.+?/.\\d+/.+?/\\d+.(\\d+).240.180.jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.duration",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.main",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "ul.suggestions-container.tag-suggestions li a",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.inner a span.title",
        "multiple": "true",
        "place": "text"
      },
      {
        "videoInfoType": "created_date",
        "classifier": "div#videoAbout p",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.gotporn.com/video/%s/embedframe\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}