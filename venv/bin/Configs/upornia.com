{
  "name": "upornia.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 48,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://upornia.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "2500",
      "lastPage": "nav#list_videos2_common_videos_list_pagination ul li.active~li"
    },
    {
      "orientation": "gay",
      "link": "https://upornia.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "310",
      "lastPage": "nav#list_videos2_common_videos_list_pagination ul li.active~li",
      "cookies": {
        "category_group_id": "68"
      }
    },
    {
      "orientation": "transsexual",
      "link": "https://upornia.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "110",
      "lastPage": "nav#list_videos2_common_videos_list_pagination ul li.active~li",
      "cookies": {
        "category_group_id": "67"
      }
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "5"
  },
  "videos": {
    "classifier": "div#list_videos2_common_videos_list_items article.item a",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".*contents/videos_screenshots/(\\d+/\\d+)/(\\d+x\\d+)/\\d+.jpg",
              "replacement": "http://direct.upornia.com/contents/videos_sources/$1/screenshots/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/contents/videos_screenshots/\\d+/\\d+/\\d+x\\d+/(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.thumbnail__info__right",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "div.thumbnail__label.thumbnail__label--left",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#holder",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.info div.item a.label-default[href*='/categories']",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.info div.item a.label-default[href*='/tags']",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.info div.item a.label.label-default[href*='/models']",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.info div.item:nth-child(2) span em",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.info div.item a.label.label-success[href*='/channels']",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://upornia.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}