{
  "name": "xhamster.com",
  "scheduler": {
    "intervalInSeconds": "86400"
  },
  "traversal": [
    {
      "orientation": "unknown",
      "pre_checks": [
        {
          "type": "content",
          "selector": "#videoClosed",
          "value": "This video is visible for"
        }
      ]
    },
    {
      "orientation": "gay",
      "pre_checks": [
        {
          "type": "content",
          "selector": "#videoClosed",
          "value": "This video is visible for"
        }
      ]
    },
    {
      "orientation": "transsexual",
      "pre_checks": [
        {
          "type": "content",
          "selector": "#videoClosed",
          "value": "This video is visible for"
        }
      ]
    }
  ],
  "thumbs": {
    "start": "2",
    "end": "20",
    "count": "10"
  },
  "videos": {
    "classifier": "div.thumb-list.thumb-list--sidebar.thumb-list--recent",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "div.video-thumb__date-added a.video-thumb__image-container.thumb-image-container",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "div.video-thumb__date-added a.video-thumb__image-container.thumb-image-container img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.video-thumb__date-added a.video-thumb__image-container.thumb-image-container img",
        "place": "attr",
        "place_name": "abs:src"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.thumb-image-container__duration",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.entity-container__block.entity-description-container",
    "info": [
      {
        "videoInfoType": "description",
        "classifier": "div.entity-description-container__description p",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "a.categories-container__item",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "a.categories-container__item:has(i.beta-star)",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "multiple": "false",
        "classifier": "a.categories-container__item:has(i.sponsor-tag)",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "div.entity-container__block.entity-description-container a.categories-container__item:contains(HD)"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://xhamster.com/xembed.php?video=%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}