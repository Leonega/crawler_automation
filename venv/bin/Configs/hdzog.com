{
  "name": "hdzog.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 48,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.hdzog.com/new/%s/",
      "start_index": "1",
      "end_index": "6210"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "2",
    "end": "40",
    "count": "10"
  },
  "videos": {
    "classifier": "div.thumbs-videos ul.cf li a",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.thumb img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/contents/.+?/(\\d+/\\d+)/(\\d+x\\d+)/\\d+.jpg",
              "replacement": "http://direct.hdzog.com/contents/videos_sources/$1/screenshots/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.thumb img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/contents/.+?/\\d+/\\d+/\\d+x\\d+/(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "div.thumb img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.thumb span.time",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.player-block",
    "info": [
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.video-description p",
        "place": "text",
        "filters": [
          {
            "filter": "replace_if_exist",
            "params": {
              "substring": "There is no description for this video",
              "value": ""
            }
          }
        ]
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "li.categories a",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "multiple": "false",
        "classifier": "div#channel-banner a img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "hd",
        "classifier": "div.player-block li.categories a:contains(HD)",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "li.tags a",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "li.models a span.name",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.hdzog.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}