{
  "name": "mcfucker.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 60,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://mcfucker.com/%s/",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "30",
    "count": "5"
  },
  "videos": {
    "classifier": "div#pg section.cv article.thumbs div.thumb",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "640x360/(\\d+?).*$",
              "replacement": "640x480/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/640x360/(\\d+?).*$",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.length",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#pg",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.tags.s2 div a ",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "title",
        "classifier": "section.cvp h1",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.tags:not(.s2) div:nth-child(2) a",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.tags:not(.s2) div:nth-child(5) a",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.vadv a img",
        "place": "attr",
        "place_name": "abs:src"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://mcfucker.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}