{
  "name": "pornoxo",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 50,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.pornoxo.com/videos/best-recent/%s/",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "15",
    "count": "5"
  },
  "videos": {
    "classifier": "li.thumb-item.videospot ",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "div.thumb.vidItem",
        "place": "attr",
        "place_name": "data-video-id"
      },
      {
        "videoInfoType": "title",
        "classifier": "img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.text-active.bold",
        "place": "text"
      },
      {
        "videoInfoType": "thumb",
        "classifier": " img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "/pxo-320x240/(.+)320x240-\\d+.jpg",
              "replacement": "/pxo-full/$1full-%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": " img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "https://.+?/thumbs/.+?/.+?/.+?/.+?.-320x240-(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.fs11.viddata.flr.time-label-wrapper span:contains(:)",
        "place": "text",
        "filters": [
          {
            "filter": "replace_if_exist",
            "params": {
              "substring": "HD ",
              "value": ""
            }
          }
        ]
      }
    ]
  },
  "video_details": {
    "classifier": "div.video-detail-tags-wrapper",
    "info": [
      {
        "videoInfoType": "tags",
        "classifier": "div.content-tags a",
        "multiple": "true",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.pornoxo.com/embed/%s/\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}