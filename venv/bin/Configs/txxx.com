{
  "name": "txxx",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "https://txxx.com/latest-updates/%s/",
      "start_index": "1",
      "lastPage": "$('div#list_videos2_common_videos_list_pagination span.paginator__item paginator__item--active ~a')"
    },
    {
      "orientation": "gay",
      "link": "https://txxx.com/latest-updates/%s/",
      "start_index": "1",
      "cookies": {
        "category_group_id": "67"
      }
    },
    {
      "orientation": "transsexual",
      "link": "https://txxx.com/latest-updates/%s/",
      "start_index": "1",
      "cookies": {
        "category_group_id": "68"
      }
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "10",
    "count": "10"
  },
  "videos": {
    "classifier": "div#list_videos2_common_videos_list",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a.image.js-thumb-pagination",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a.image.js-thumb-pagination",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.thumb__inner",
        "place": "attr",
        "place_name": "style",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/contents/videos_screenshots/(\\d+/\\d+)/\\d+x\\d+/\\d+.jpg.*",
              "replacement": "https://direct.txxx.com/contents/videos_sources/$1/screenshots/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.thumb__inner",
        "place": "attr",
        "place_name": "style",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/contents/videos_screenshots/\\d+/\\d+/\\d+x\\d+/(\\d+).jpg.*",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.thumb__duration",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.thumb__hd"
      },
      {
        "videoInfoType": "vr",
        "classifier": "span.thumb__hd.thumb__hd--vr"
      },
      {
        "videoInfoType": "title",
        "classifier": "a.thumb__title.thumb__title--left.title",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#video-info-tabs-id-tab-0",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.video-info__mark a[href*='/categories']",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.video-info__mark a[href*='/tags']",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.video-info__mark a[href*='/models'] span",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "multiple": "true",
        "classifier": "a.video-loader__name.link span",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.video-info__desc p.video-info__desc-text",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"http://txxx.com/embed/%s\" width=\"640\" height=\"480\" frameborder=\"0\" scrolling=\"no\" allowfullscreen></iframe>"
}