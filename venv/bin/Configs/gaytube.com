{
  "name": "gaytube.com",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "gay",
      "link": "https://www.gaytube.com/c/all/?p=%s",
      "start_index": "1",
      "end_index": "1",
      "lastPage": "ul.pagination li.active~li"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "2",
    "end": "15",
    "count": "10"
  },
  "videos": {
    "classifier": "div.v-preview.p-r a.img",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "media/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.rotating-img-container",
        "place": "attr",
        "place_name": "datasrc"
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.rotating-img-container",
        "place": "attr",
        "place_name": "datasrc",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?/m=.+?/media/.+?/.+?(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.v-length",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "section.video-details.margin-top-25 ",
    "info": [
      {
        "videoInfoType": "description",
        "classifier": "div.more-info-item.description.font-13",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.more-info-item.margin-top-10 a div.font-bold.category-btn",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.more-info-item.margin-top-10 a.category-btn",
        "place": "text"
      }
    ]
  }
}