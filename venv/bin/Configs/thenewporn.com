{
  "name": "thenewporn.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 90,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://www.thenewporn.com/%s/",
      "start_index": "1",
      "end_index": "1",
      "lastPage": "nav#pgn a.active ~a"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "23",
    "count": "5"
  },
  "videos": {
    "classifier": "div#pG section#content div.wrp article#video div.ic a",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.img img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "240x180/(\\d+?).*$",
              "replacement": "640x480/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.img img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/240x180/(\\d+?).*$",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "video_details": {
    "classifier": "article#item-info",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.tags_block div.tag a ",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "li.models p a",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "li.categories p a",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.description",
        "place": "text",
        "filters": [
          {
            "filter": "replace_if_exist",
            "params": {
              "substring": "Sorry, there is no description for this video",
              "value": ""
            }
          }
        ]
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.left div.discount div.imgbg img",
        "place": "attr",
        "place_name": "alt"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://thenewporn.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}