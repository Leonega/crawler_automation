{
  "name": "pornoid.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 40,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://pornoid.com/%s",
      "start_index": "1",
      "lastPage": "nav#pgn a.active ~a"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "30",
    "count": "10"
  },
  "videos": {
    "classifier": "div#wrapper section#content article#video div.ic a",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.img span",
        "place": "text"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.img img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "320x180/(\\d+?).*$",
              "replacement": "640x480/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.img img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/320x180/(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "video_details": {
    "classifier": "article#item-info",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.p20 > div:nth-child(3) a",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.p20 a[href*=\"performers\"]",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.data-tags div.tags a",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.data-tags p",
        "place": "text",
        "filters": [
          {
            "filter": "replace_if_exist",
            "params": {
              "substring": "Sorry, there is no description for this video",
              "value": ""
            }
          }
        ]
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.title font",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://pornoid.com/embed/%s/\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}