{
  "name": "91porn.com",
  "botCheckMs": "0",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "http://91porn.com/video.php?category=rf&page=%s"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "36000"
  },
  "thumbs": {
    "start": "1",
    "end": "10",
    "count": "10"
  },
  "videos": {
    "classifier": "div#videobox table td div.listchannel",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "div > a",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_after",
            "params": {
              "substring": "&page"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div > a > img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(http://.+?/thumb/)\\d+(_.+?).jpg",
              "replacement": "$1%s$2.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div > a > img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "http://.+?/thumb/(\\d+)_.+?.jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "hd",
        "classifier": "img.png-over[src='/images/hd.png']"
      },
      {
        "videoInfoType": "id",
        "classifier": "div > a > img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "thumb/"
            }
          },
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+_(\\d+)\\.jpg$",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span:containsOwn(Runtime)",
        "place": "nextTextSibling"
      },
      {
        "videoInfoType": "title",
        "classifier": "div > a > img",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "video_details": {}
}