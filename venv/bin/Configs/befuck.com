{
  "name": "befuck.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 40,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://www.befuck.com/%s",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "30",
    "count": "5"
  },
  "videos": {
    "classifier": "section#content article#video div.ic a",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.img img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "378x305/\\d+?.*$",
              "replacement": "640x480/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.img img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/378x305/(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.img span",
        "place": "text"
      },
      {
        "videoInfoType": "title",
        "classifier": "",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "video_details": {
    "classifier": "section#content div.cntv > div.info",
    "info": [
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": " div.origin p.models a",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": " div.origin p.cats a",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": " div.origin p.tags a",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.tools span b",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://befuck.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}