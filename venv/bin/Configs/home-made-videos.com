{
  "name": "home-made-videos",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "http://www.home-made-videos.com/"
    },
    {
      "orientation": "straight",
      "link": "http://www.home-made-videos.com/page/%s/",
      "start_index": "2",
      "lastPage": "ul#pagination li.active~li"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "1"
  },
  "videos": {
    "classifier": "div#list_videos_most_recent_videos div.item",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "a div.img img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.wrap div.duration",
        "place": "text",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": ": "
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a div.img img",
        "place": "attr",
        "place_name": "abs:src"
      }
    ]
  },
  "video_details": {
    "classifier": "div.container ",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": " div:nth-child(16) > a:nth-child(2) ",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "h2.post-title",
        "place": "text"
      }
    ]
  }
}