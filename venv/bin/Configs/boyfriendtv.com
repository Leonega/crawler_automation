{
  "name": "boyfriendtv.com",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "gay",
      "link": "https://www.boyfriendtv.com/videos/newest/%s/",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "15",
    "count": "5"
  },
  "videos": {
    "classifier": "li.thumb-item.videospot div.thumb.vidItem",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "data-video-id"
      },
      {
        "videoInfoType": "title",
        "classifier": "a img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "/bftv-\\d+x\\d+/(\\d+-\\d+)/(.+)-\\d+x\\d+-\\d+.jpg",
              "replacement": "/bftv-full/$1/$2-full-%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "https://.+?/thumbs/.+?-320x240/\\d+-\\d+/.+?/.+?.mp4-320x240-(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.fs11.viddata.flr",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.text-active.bold",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.bottom-info-box ",
    "info": [
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "a.model-card",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.content-tags a.tag-item",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.boyfriendtv.com/embed/%s/\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}