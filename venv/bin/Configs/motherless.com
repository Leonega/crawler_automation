{
  "name": "motherless",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "http://motherless.com/videos/recent?page=%s",
      "start_index": "1",
      "lastPage": "div.pagination_link span.current~span"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "1"
  },
  "videos": {
    "classifier": "div.thumb.video.medium",
    "info": [
      {
        "videoInfoType": "id",
        "place": "attr",
        "place_name": "data-codename"
      },
      {
        "videoInfoType": "link",
        "classifier": "a.img-container",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "a.img-container img.static",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "created_date",
        "classifier": "div.captions div:last-child",
        "place": "text"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a.img-container img.static",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://(.+?)/thumbs/(.+?)-.+?.jpg\\?from_helper",
              "replacement": "https://$1/thumbs/$2.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.caption.left",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#media-info",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div#media-tags-container a.pop",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div#media-groups-container a.pop",
        "place": "text"
      }
    ]
  }
}