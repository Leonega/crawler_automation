{
  "name": "xtube.com",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 50,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://www.xtube.com/video/%s",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "2",
    "end": "10",
    "count": "15"
  },
  "videos": {
    "classifier": "section#mainSection a.titleRemover",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "/?./?.+\\-(\\d+)",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://(.+?/.+?/.+?/.+?/.+?/.+?/.+?)/\\d+.jpg",
              "replacement": "$1/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?/.+?/.+?/.+?/.+?/.+?/.+?/(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.videoDuration",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "i.icon_hd",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "section#mainSection",
    "info": [
      {
        "videoInfoType": "description",
        "classifier": "span.description.show",
        "place": "text"
      },
      {
        "videoInfoType": "title",
        "classifier": "form.underPlayerRateForm.rate.rateForm.clean.btn-group.btn-group-justified h1",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.categories li a",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.tags li a",
        "place": "text"
      }
    ]
  }
}