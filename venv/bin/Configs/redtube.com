{
  "name": "redtube.com",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.redtube.com/?page=%s",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "2",
    "end": "16",
    "count": "10",
    "formats": [
      {
        "preCheckInfo": {
          "preCheck": "contains",
          "params": {
            "substring": "o.jpg"
          }
        },
        "padding": "true",
        "minLength": "3",
        "padChar": "0"
      }
    ]
  },
  "videos": {
    "classifier": "div#browse_section div.video_thumb_title_wrap a",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "img",
        "place": "attr",
        "place_name": "id",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?_(\\d+)",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "HD",
        "classifier": "span.hd-video-text",
        "place": "text"
      },
      {
        "videoInfoType": "VR",
        "classifier": "span.vr-video",
        "place": "text"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.duration",
        "place": "text",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?\\s|(\\d+\\:\\d+)",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?/.+?/.+?/.+?/\\d+/\\d+/\\d+/.+?/(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://(.+?)/.+?/videos/(\\d+/\\d+/\\d+)/original/\\d+.jpg",
              "replacement": "https://$1/media/videos/$2/original/%s.jpg"
            }
          }
        ]
      }
    ]
  },
  "video_details": {
    "classifier": "div#video-infobox-wrap",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.video-infobox-col div.video-infobox-content a",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.video-infobox-row.video-infobox-hidden-row a",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.pornstar-name.pornstarPopupWrapper a",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.video-infobox-content a.video-infobox-link",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://embed.redtube.com/?id=%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}