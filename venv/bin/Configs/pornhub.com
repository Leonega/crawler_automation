{
  "name": "pornhub.com",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 264,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.pornhub.com/video?page=%s",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "2",
    "end": "16",
    "count": "5"
  },
  "videos": {
    "classifier": "ul#videoCategory li",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "div.thumbnail-info-wrapper.clearfix a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "li.js-pop.videoblock.videoBox",
        "place": "attr",
        "place_name": "data-id"
      },
      {
        "videoInfoType": "title",
        "classifier": "div.img.videoPreviewBg a.img img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.img.videoPreviewBg a.img > img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "https://.+?/videos/\\d+/\\d+/\\d+/.+?/.+?(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "var.duration",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.hd-thumbnail",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "head",
    "info": [
      {
        "videoInfoType": "thumb",
        "classifier": "meta[property='og:image']",
        "place": "attr",
        "place_name": "abs:content",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(.+?)\\d+.jpg",
              "replacement": "$1%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.video-detailed-info div.video-info-row.showLess div.tagsWrapper > a",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.video-detailed-info div.categoriesWrapper > a",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": " div.video-info-row div.pornstarsWrapper > a.pstar-list-btn.js-mxp",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": " div.video-info-row div.usernameWrap.clearfix a.bolded",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.pornhub.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}