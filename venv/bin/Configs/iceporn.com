{
  "name": "iceporn.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 88,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.iceporn.com/videos/%s",
      "start_index": "2",
      "end_index": "1300",
      "lastPage": "div.paginate ul li.active_page ~ li"
    },
    {
      "orientation": "gay",
      "link": "https://www.iceporn.com/videos/%s",
      "start_index": "2",
      "end_index": "400",
      "cookies": {
        "cattype": "gay"
      },
      "lastPage": "div.paginate ul li.active_page ~ li"
    },
    {
      "orientation": "transsexual",
      "link": "https://www.iceporn.com/videos/%s",
      "start_index": "2",
      "end_index": "90",
      "cookies": {
        "cattype": "trans"
      },
      "lastPage": "div.paginate ul li.active_page ~ li"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "10"
  },
  "videos": {
    "classifier": "div.maintenance div.prime_hold div.sheave > a",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "video/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https?://(.+?)/media/videos/tmb/(\\d+)/(\\d+)_(\\d+)/.*$",
              "replacement": "https://$1/media/videos/tmb/$2/player/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/media/videos/tmb/\\d+/\\d+_\\d+/(\\d+).*$",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "hd",
        "classifier": "i.hd"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "i.time",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.watch div.base",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.data_categories a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "title",
        "classifier": "div.caption h2",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.data > span > em:contains(Website) a",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.iceporn.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}