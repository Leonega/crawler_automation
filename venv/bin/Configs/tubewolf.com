{
  "name": "tubewolf",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "http://www.tubewolf.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "1",
      "lastPage": "ul.pagination li.active~li"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "20"
  },
  "videos": {
    "classifier": "div.thumb-list div.thumb",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a.kt_imgrc",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a.kt_imgrc img.th",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "https://img.+?-tw.alphaxcdn.com/\\d+/(\\d+)/240x180/\\d.jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "a.kt_imgrc",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a.kt_imgrc img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?/(.+?/.+?)/\\d+x\\d+/\\d+.jpg",
              "replacement": "https://img3-tw.alphaxcdn.com/$1/240x180/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a.kt_imgrc img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?/.+?/.+?/\\d+x\\d+/(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.duration",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#content",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.row:contains(Tags) a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.row:contains(Pornstar) a",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "multiple": "true",
        "classifier": "div.row:contains(Channel) a",
        "place": "text"
      }
    ]
  }
}