{
  "name": "fuckler",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "http://fuckler.com/new/%s/",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "5"
  },
  "videos": {
    "classifier": "div.thumb-content a.thumb-img",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "video/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "thumb",
        "classifier": " img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(.+?/.+?/.+?/.+?/\\d+/.+?-)\\d+.jpg",
              "replacement": "$1%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": " img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/.+?/.+?/.+?/\\d+/.+?-(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.thumb-length",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.page-follow",
    "info": [
      {
        "videoInfoType": "paysite",
        "classifier": "a",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://fuckler.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}