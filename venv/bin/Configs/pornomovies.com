{
  "name": "pornomovies",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "https://www.pornomovies.com/most-recent/page%s.html",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "30",
    "count": "10"
  },
  "videos": {
    "classifier": "div.item-block.item-normal.col",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "div.inner-block a ",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "com/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "-"
            }
          }
        ]
      },
      {
        "videoInfoType": "link",
        "classifier": "div.inner-block a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.inner-block a span.image img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(https://.+?/thumbs/.+?/.+?/.+?/.+?/.+?/.+?/.+?.mp4-)\\d+.jpg",
              "replacement": "$1%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.inner-block a span.image img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "https://.+?/thumbs/.+?/.+?/.+?/.+?/.+?/.+?/.+?.mp4-(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "div.inner-block a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.video-hd-mark",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#stats div.inner-block ",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.stats-content.stats-content--right span.sub-desc a",
        "place": "text"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "ul.stats-list.stats-list--plain li:nth-child(2) span.sub-desc",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.pornomovies.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}