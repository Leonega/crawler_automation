{
  "name": "spankwire.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.spankwire.com/recentvideos/Straight?page=%s",
      "start_index": "1",
      "end_index": "10835",
      "cookies": {
        "sexpref": "Straight"
      }
    },
    {
      "orientation": "gay",
      "link": "https://www.spankwire.com/recentvideos/Gay?page=%s",
      "start_index": "1",
      "end_index": "3604"
    },
    {
      "orientation": "transsexual",
      "link": "https://www.spankwire.com/recentvideos/Tranny?page=%s",
      "start_index": "1",
      "end_index": "728"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "16",
    "count": "10"
  },
  "videos": {
    "classifier": "div#js-react-indexed-videos-list",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "div.sc-lkqHmb.hqdFRN a.sc-hwwEjo.bVuCil",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "div.sc-lkqHmb.hqdFRN a.sc-hwwEjo.bVuCil img.sc-eTuwsz.cmVOWl",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.sc-lkqHmb.hqdFRN a.sc-hwwEjo.bVuCil img.sc-eTuwsz.cmVOWl",
        "place": "attr",
        "place_name": "abs:src"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.sc-ibxdXY.iBHZWC",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "div.sc-kIPQKe.ecIRQA span",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#videoContainer",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "span.catTagInfo > a",
        "place": "text"
      },
      {
        "videoInfoType": "id",
        "classifier": "div#js-react-video-player",
        "place": "attr",
        "place_name": "data-video-id"
      },
      {
        "videoInfoType": "created_date",
        "classifier": "div.uploaded",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.spankwire.com/EmbedPlayer.aspx?ArticleId=%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}