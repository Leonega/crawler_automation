{
  "name": "homepornking.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 70,
  "traversal": [
    {
      "orientation": "straight",
      "link": "https://www.homepornking.com/new/"
    },
    {
      "orientation": "straight",
      "link": "https://www.homepornking.com/new/%s/",
      "start_index": "2",
      "end_index": "1089"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "15",
    "count": "10"
  },
  "videos": {
    "classifier": "div.pics a",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+/thumbs/(.+)/\\d+.*$",
              "replacement": "https://www.homepornking.com/video/$1/"
            }
          }
        ]
      },
      {
        "videoInfoType": "id",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+/thumbs/\\d+/\\d+/\\d+/(\\d+)/\\d+.*$",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "/thumbs/(.+)/\\d+.jpg*$",
              "replacement": "/thumbs/$1/%sb.jpg"
            }
          }
        ]
      }
    ]
  },
  "video_details": {
    "classifier": "div.videoplayer",
    "info": [
      {
        "videoInfoType": "title",
        "classifier": "div.vtitle h2",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.tags a",
        "place": "text"
      }
    ]
  }
}