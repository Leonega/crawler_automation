{
  "name": "hclips.com",
  "botCheckMs": "1",
  "stopNumberExistingVideosInRow": 80,
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "10",
    "count": "10"
  },
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.hclips.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "1"
    },
    {
      "orientation": "gay",
      "link": "https://www.hclips.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "1",
      "cookies": {
        "category_group_id": "2"
      }
    },
    {
      "orientation": "transsexual",
      "link": "https://www.hclips.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "1",
      "cookies": {
        "category_group_id": "4"
      }
    }
  ],
  "video_details": {
    "info": []
  },
  "embedded_template": "<iframe src=\"https://hclips.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}