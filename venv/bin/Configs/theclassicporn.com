{
  "name": "theclassicporn.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://theclassicporn.com/videos/%s/?vcids=6027",
      "start_index": "1",
      "end_index": "10"
    },
    {
      "orientation": "gay",
      "link": "https://gay.theclassicporn.com/videos/%s/?vcids=6027",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "10"
  },
  "videos": {
    "classifier": "div.content-block.jsVideosListFull ul.videos-list div.video-list-item",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a.thumb-video-link",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a.thumb-video-link",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "p.title a",
        "place": "text"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "ul.info-bar li:nth-child(4)",
        "place": "text",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "Duration: "
            }
          }
        ]
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "p.data-row.data-actress a",
        "place": "text"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.video-screens ul.cf li img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/contents/videos_screenshots/(\\d+/\\d+)/\\d+x\\d+/\\d+.jpg",
              "replacement": "http://direct.theclassicporn.com/contents/videos_sources/$1/screenshots/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.video-screens ul.cf li img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/contents/videos_screenshots/\\d+/\\d+/\\d+x\\d+/(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      }
    ]
  },
  "video_details": {
    "classifier": "div.video-info-top",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "p.data-row.data-categories a.tag-button",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "classifier": "div.description.video-desc.jsVideoDesc",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://theclassicporn.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}