{
  "name": "overthumbs",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "http://overthumbs.com/date/%s/",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "5"
  },
  "videos": {
    "classifier": "div.v-content02.aft a.th-video",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "id",
        "classifier": " img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "http://.+?/content/.+?/.+?/.+?/(\\d+)/.+?.jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": " img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(http://.+?/content/.+?/.+?/.+?/\\d+/.+?-)\\d+.jpg",
              "replacement": "$1%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": " img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "http://.+?/content/.+?/.+?/.+?/\\d+/.+?-(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.time",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.descr-sponsor",
    "info": [
      {
        "videoInfoType": "paysite",
        "classifier": "img",
        "place": "attr",
        "place_name": "alt"
      }
    ]
  }
}