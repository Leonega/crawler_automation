{
  "name": "alphaporno.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 101,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://www.alphaporno.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "6",
      "lastPage": "nav.pagination-bottom ul li.active~li"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "5",
    "count": "5"
  },
  "videos": {
    "classifier": "div.main-box li.thumb",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "https?://.+?/\\d+/(\\d+)/\\d+x\\d+/\\d+.jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https?://(.+?/.+?)/.+?/(\\d+/\\d+)/.+?/\\d+.jpg",
              "replacement": "http://$1/videos_sources/$2/screenshots/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https?://.+?/\\d+/\\d+/\\d+x\\d+/(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.duration",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.video-box",
    "info": [
      {
        "videoInfoType": "title",
        "classifier": "h1.title",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "nav.player-nav.tags ul li:not(:first-child) a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "hd",
        "classifier": "nav.player-nav.tags ul li:not(:first-child) a:contains(HD)",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "nav.player-nav.tags ul li a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "nav.player-nav.models ul li a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.content-provider a img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "created_date",
        "classifier": "ul.tools-list li.date",
        "place": "ownText"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.alphaporno.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}