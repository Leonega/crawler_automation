{
  "name": "nuvid.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 40,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.nuvid.com/videos/%s",
      "start_index": "1",
      "end_index": "50"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "5"
  },
  "videos": {
    "classifier": "div#search_results div.box-tumb.related_vid a.th.video-thumb.vid_link.ch-video",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "video/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https?://(.+?)/media/videos/tmb/(\\d+)/(\\d+)_(\\d+)/.*$",
              "replacement": "http://$1/media/videos/tmb/$2/player/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/media/videos/tmb/\\d+/\\d+_\\d+/(\\d+).*$",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.container_image i.time",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "i.hd",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#main-content",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.video-cat:contains(Categories) a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.views a",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.video-cat:contains(Models) a",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.nuvid.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}