{
  "name": "wetplace.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 40,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://www.wetplace.com/%s/",
      "start_index": "1",
      "lastPage": "nav#pgn a.active ~a",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "27",
    "count": "5"
  },
  "videos": {
    "classifier": "div#icnt.video.itemgrid article",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a.link",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a.link",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.img img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "240x180/(\\d+?).*$",
              "replacement": "640x480/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.img img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/240x180/(\\d+?).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "a.link",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "video_details": {
    "classifier": "div#ocnt.video_splash",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.tags a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.info",
        "place": "text",
        "filters": [
          {
            "filter": "replace_if_exist",
            "params": {
              "substring": "Sorry, there is no description for this video",
              "value": ""
            }
          }
        ]
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.discount div.imgbg a img",
        "place": "attr",
        "place_name": "alt"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://wetplace.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}