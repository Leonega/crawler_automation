{
  "name": "drtuber.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 80,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.drtuber.com/videos/%s",
      "start_index": "1",
      "end_index": "150"
    },
    {
      "orientation": "gay",
      "link": "https://www.drtuber.com/videos/%s",
      "start_index": "1",
      "end_index": "130",
      "cookies": {
        "cattype": "gay"
      }
    },
    {
      "orientation": "transsexual",
      "link": "https://www.drtuber.com/videos/%s",
      "start_index": "1",
      "end_index": "9",
      "cookies": {
        "cattype": "trans"
      }
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "10"
  },
  "videos": {
    "classifier": "div.thumbs_box div.thumbs a",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "/media/videos/tmb/(\\d+)/(\\d+)_(\\d+)/.*$",
              "replacement": "/media/videos/tmb/$1/player/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/media/videos/tmb/\\d+/\\d+_\\d+/(\\d+).*$",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "strong.toolbar em.time_thumb em",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "i.ico_hd"
      }
    ]
  },
  "video_details": {
    "classifier": "div.video_box div.video",
    "info": [
      {
        "videoInfoType": "id",
        "place": "attr",
        "place_name": "data-vid"
      },
      {
        "videoInfoType": "title",
        "classifier": "h1.title",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.categories_list a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "span.autor_web a",
        "place": "text"
      },
      {
        "videoInfoType": "created_date",
        "classifier": "span.autor",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "span.autor.models a",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.drtuber.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}