{
  "name": "pornalized.com",
  "stopNumberExistingVideosInRow": 40,
  "botCheckMs": "300",
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://pornalized.com/%s/",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "30",
    "count": "5"
  },
  "videos": {
    "classifier": "section#content section.items.videos div.ic a",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.imgroll img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "640x360/(\\d+?).*$",
              "replacement": "640x480/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.imgroll img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/640x360/(\\d+).*$",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "video_details": {
    "classifier": "div#mZoom",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.vInfo div.origin p:nth-child(1) a",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.discount div.block a img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.vInfo div.origin p:nth-child(2) a",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://pornalized.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}