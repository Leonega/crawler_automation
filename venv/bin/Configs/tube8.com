{
  "name": "tube8.com",
  "botCheckMs": "40",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.tube8.com/newest/page/%s/",
      "start_index": "1",
      "end_index": "401"
    },
    {
      "orientation": "gay",
      "link": "https://www.tube8.com/gay/newest/page/%s/",
      "start_index": "1",
      "end_index": "90"
    },
    {
      "orientation": "transsexual",
      "link": "https://www.tube8.com/shemale/newest/page/%s/",
      "start_index": "1",
      "end_index": "20"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "2",
    "end": "16",
    "count": "10"
  },
  "videos": {
    "classifier": "div#category_video_list div.thumb_box",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a.video-thumb-link",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a.video-thumb-link img.js_lazy ",
        "place": "attr",
        "place_name": "id",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "i"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "a img.js_lazy.videoThumbs.js-flipBook ",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a img.js_lazy.videoThumbs.js-flipBook",
        "place": "attr",
        "place_name": "abs:data-thumb",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(https://.+?)/(\\d+/\\d+/\\d+)/(originals?)/\\d+.+?.jpg|(https://.+?)/(videos/\\d+/\\d+/\\d+)/(originals?)/\\d+.+?.jpg|(https://.+?)/(videos/\\d+/\\d+/\\d+)/(thumbs_\\d+)/.+?\\d+.jpg|(https://.+?)/(videos/\\d+/\\d+/\\d+)/(originals?)/.+?\\d+.jpg",
              "replacement": "$1/$2/$3/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a img.js_lazy.videoThumbs.js-flipBook",
        "place": "attr",
        "place_name": "abs:data-thumb",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?/\\d+/\\d+/\\d+/originals?/(\\d+).+?.jpg|https://.+?/videos/\\d+/\\d+/\\d+/originals?/(\\d+).jpg|https://.+?/videos/\\d+/\\d+/\\d+/thumbs_\\d+/\\.+?(\\d+).jpg|https://.+?/videos/\\d+/\\d+/\\d+/originals?/.+?(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.video_duration",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "div.video_info span.hdIcon",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.infoBoxInnerToggle",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "dl.tags dd div.list span.item > a",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.category dl dd > a",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.row",
        "place": "text",
        "filters": [
          {
            "filter": "replace_if_exist",
            "params": {
              "substring": "Description: ",
              "value": ""
            }
          }
        ]
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "dd#btnBlueAddPs a",
        "place": "text",
        "filters": [
          {
            "filter": "replace_if_exist",
            "params": {
              "substring": "Add Pornstar",
              "value": ""
            }
          }
        ]
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.tube8.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}