{
  "name": "hd21.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 88,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://www.hd21.com/videos/%s",
      "start_index": "1",
      "end_index": "180",
      "cookies": {
        "lang": "en"
      }
    },
    {
      "orientation": "gay",
      "link": "http://www.hd21.com/videos/%s",
      "start_index": "1",
      "end_index": "1",
      "cookies": {
        "cattype": "gay",
        "lang": "en"
      }
    },
    {
      "orientation": "transsexual",
      "link": "http://www.hd21.com/videos/%s",
      "start_index": "1",
      "end_index": "1",
      "cookies": {
        "cattype": "trans",
        "lang": "en"
      }
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "10"
  },
  "videos": {
    "classifier": "div.container div.video_thumbs",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "div.video_thumb a.th",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "div.video_thumb a.th",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "video/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "span.wrap img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(.+?)/tmb/(\\d+)/(\\d+)_(\\d+)/.*$",
              "replacement": "$1/tmb/$2/player/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/media/videos/tmb/\\d+/\\d+_\\d+/(\\d+).*$",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.time",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.quality span",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.main_block.black",
    "info": [
      {
        "videoInfoType": "title",
        "classifier": "div.player_tools div.title",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.player_tools div.tags a.item",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.player_tools div.col span a span",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"http://www.hd21.com/embed/%s/\" width=\"640\" height=\"480\" frameborder=\"0\" scrolling=\"no\" allowfullscree></iframe>"
}