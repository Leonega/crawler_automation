{
  "name": "submityourflicks",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "http://www.submityourflicks.com/newest/page%s.html",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "30",
    "count": "10"
  },
  "videos": {
    "classifier": "div.item-block.item-normal.col a ",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": " ",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": ".com/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "-"
            }
          }
        ]
      },
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "thumb",
        "classifier": " span.image img",
        "place": "attr",
        "place_name": "data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?.mp4/.+?.mp4-(\\d+).jpg",
              "replacement": "https://thumbs.submityourflicks.com/media/thumbs/9/6/6/2/5/966258a2140462fa0.mp4/966258a2140462fa0.mp4-%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": " span.image img",
        "place": "attr",
        "place_name": "data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?.mp4/.+?.mp4-(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": " img",
        "place": "attr",
        "place_name": "alt"
      }
    ]
  },
  "video_details": {
    "classifier": "section#main-column ",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "span.sub-desc a",
        "place": "text"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "ul.stats-list.stats-list--plain span.sub-desc:contains(:)",
        "place": "text",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(\\d+:\\d+).+",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.inner-block p",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.submityourflicks.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}