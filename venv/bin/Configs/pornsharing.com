{
  "name": "pornsharing.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 100,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://pornsharing.com/videos/page_%s",
      "start_index": "1",
      "end_index": "828",
      "timeout": "0"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "36000"
  },
  "thumbs": {
    "start": "1",
    "end": "31",
    "count": "10"
  },
  "videos": {
    "classifier": "div.item div.toolbar",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "a.img",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "_v"
            }
          }
        ]
      },
      {
        "videoInfoType": "hd",
        "classifier": "div.hd"
      },
      {
        "videoInfoType": "link",
        "classifier": "a.img",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "a.head",
        "place": "text"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a.img img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://(.+?)/\\d+x\\d+/\\d+.jpg",
              "replacement": "https://$1/fullsize/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a.img img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?/\\d+x\\d+/(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      }
    ]
  },
  "video_details": {
    "classifier": "div.add-info",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.cats > a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "description",
        "classifier": "div.comment",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.models div a",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.sitelink img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.tags nav > a",
        "place": "attr",
        "place_name": "title"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://pornsharing.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}