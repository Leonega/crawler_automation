{
  "name": "zedporn",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "http://zedporn.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "5"
  },
  "videos": {
    "classifier": "div.wrap div.smovies-list ul li.thumb",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a span.img-shadow img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "https://.+?/\\d+/(\\d+)/240x180/\\d+.jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(https?://img\\d+-zp.alphaxcdn.com/\\d+/\\d+/240x180/)\\d+.jpg",
              "replacement": "$1%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "https?://img\\d+-zp.alphaxcdn.com/\\d+/\\d+/240x180/(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.intro_th span.duration",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.wrapper_watch",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "nav.niches-nav.wrap div.tags ul li a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "nav.niches-nav.wrap div.category a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "nav.niches-nav.wrap div.category a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.content-provider a img",
        "place": "attr",
        "place_name": "alt"
      }
    ]
  }
}