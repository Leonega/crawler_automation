{
  "name": "voyeurhit",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 60,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://voyeurhit.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "18"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "2",
    "end": "15",
    "count": "5"
  },
  "videos": {
    "classifier": "div.container div.block-thumb a.thumb",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "img",
        "place": "attr",
        "place_name": "data-video-id"
      },
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".*contents/videos_screenshots/(\\d+/\\d+)/\\d+x\\d+/\\d+.jpg",
              "replacement": "http://direct.voyeurhit.com/contents/videos_sources/$1/screenshots/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/contents/videos_screenshots/\\d+/\\d+/\\d+x\\d+/(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.dur_ovimg",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "li#videoDataTabInfo",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "ul li.categories a",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "ul li.tags a",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": " div.info-player p",
        "place": "text",
        "filters": [
          {
            "filter": "replace_if_exist",
            "params": {
              "substring": "Sorry, there is no description for this video",
              "value": ""
            }
          }
        ]
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://voyeurhit.com/embed/%s/\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}