{
  "name": "viptube.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 88,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://www.viptube.com/videos/%s",
      "start_index": "1",
      "end_index": "750",
      "lastPage": "ul.paging li.active_page ~ li"
    },
    {
      "orientation": "gay",
      "link": "http://www.viptube.com/videos/%s",
      "start_index": "1",
      "end_index": "200",
      "lastPage": "ul.paging li.active_page ~ li",
      "cookies": {
        "cattype": "gay"
      }
    },
    {
      "orientation": "transsexual",
      "link": "http://www.viptube.com/videos/%s",
      "start_index": "1",
      "end_index": "30",
      "lastPage": "ul.paging li.active_page ~ li",
      "cookies": {
        "cattype": "trans"
      }
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "10"
  },
  "videos": {
    "classifier": "div.stem div.primary a.th.related_",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "video/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https?://(.+?)/media/videos/tmb/(\\d+)/(\\d+)_(\\d+)/.*$",
              "replacement": "http://$1/media/videos/tmb/$2/player/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/media/videos/tmb/\\d+/\\d+_\\d+/(\\d+).*$",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "i.time2",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "i.hd",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#mtab1",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.data_categories a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "span.from_website em a",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.viptube.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}