{
  "name": "fantasti.cc",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://fantasti.cc/videos/uploads/page_%s",
      "start_index": "2",
      "end_index": "2"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "16",
    "count": "10"
  },
  "videos": {
    "classifier": "div#featured_videos > p",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+/(\\d+)/\\Z",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "span > i",
        "place": "text"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span",
        "place": "text",
        "filters": [
          {
            "filter": "trim_after",
            "params": {
              "substring": "]"
            }
          },
          {
            "filter": "trim_before",
            "params": {
              "substring": "["
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(.+)/widget/(.+)_\\d+.jpg",
              "replacement": "$1/content/$2_%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+/widget/.+_(\\d+).jpg",
              "replacement": "$1"
            }
          }
        ]
      }
    ]
  },
  "video_details": {
    "classifier": "div.tags-list",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.tag-wrapper a",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"http://fantasti.cc/embed/%s/\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}