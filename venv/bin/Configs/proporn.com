{
  "name": "proporn.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 88,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.proporn.com/videos/%s",
      "start_index": "1",
      "end_index": "90",
      "lastPage": "li:has(span.article-active) ~ li",
      "cookies": {
        "lang": "en"
      }
    },
    {
      "orientation": "gay",
      "link": "https://www.proporn.com/videos/%s",
      "lastPage": "li:has(span.article-active) ~ li",
      "start_index": "1",
      "end_index": "1",
      "cookies": {
        "cattype": "gay",
        "lang": "en"
      }
    },
    {
      "orientation": "transsexual",
      "link": "https://www.proporn.com/videos/%s",
      "start_index": "1",
      "end_index": "2",
      "lastPage": "li:has(span.article-active) ~ li",
      "cookies": {
        "cattype": "trans",
        "lang": "en"
      }
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "10"
  },
  "videos": {
    "classifier": "div.contain div.videos div.thumb-view div.thcovering-video a.covering",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "video/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(.+?)/media/videos/tmb/(\\d+)/(\\d+)_(\\d+)/.*$",
              "replacement": "$1/media/videos/tmb/$2/player/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/media/videos/tmb/\\d+/\\d+_\\d+/(\\d+).*$",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.time-desc",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.hd"
      }
    ]
  },
  "video_details": {
    "classifier": "div.thr-ot.hid div.contain",
    "info": [
      {
        "videoInfoType": "title",
        "classifier": "h2.box-mt-output",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.it-cat-coveringcontent a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.it-sponsor-site-covering a img",
        "place": "attr",
        "place_name": "alt"
      }
    ]
  }
}