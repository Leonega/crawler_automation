{
  "name": "fuckdy.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 80,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://fuckdy.com/page%s.html",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "36000"
  },
  "thumbs": {
    "start": "2",
    "end": "10",
    "count": "5"
  },
  "videos": {
    "classifier": "ul.listThumbs div.picture",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a.thumb",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "a.thumb",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "400x225/(\\d+/\\d+)/\\d+.jpg",
              "replacement": "original/$1/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "https://.+?/.+?/.+?/.+?/(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.time",
        "place": "text"
      },
      {
        "videoInfoType": "id",
        "classifier": "div.fav",
        "place": "attr",
        "place_name": "data-id"
      },
      {
        "videoInfoType": "hd",
        "classifier": "div.hd"
      }
    ]
  },
  "video_details": {
    "classifier": "div.tube-inner",
    "info": [
      {
        "videoInfoType": "description",
        "classifier": "div.textvideo p",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "li.link a",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.textvideo li a[href*=search]",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.textvideo li a[href*=pornstars]",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"http://fuckdy.com/embed/%s/\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}