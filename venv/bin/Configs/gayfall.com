{
  "name": "gayfall",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "gay",
      "link": "http://gayfall.com/latest-updates/%s/",
      "start_index": "1",
      "lastPage": "ul.pagination li.active~li"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "20"
  },
  "videos": {
    "classifier": "div.video-list div.video-thumb",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a.kt_imgrc",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": " a.kt_imgrc img.thumb",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "http://gayfall.com/contents/videos_screenshots/\\d+/(\\d+)/209x120/\\d.jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "a.kt_imgrc",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a.kt_imgrc img",
        "place": "attr",
        "place_name": "abs:src"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.time",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.pbox-center",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "ul.fake-tags li a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.under-player-ads.adv-for-paysites a  ",
        "place": "attr",
        "place_name": "title"
      }
    ]
  }
}