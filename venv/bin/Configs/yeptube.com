{
  "name": "yeptube.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 88,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://www.yeptube.com/videos/%s",
      "start_index": "1",
      "end_index": "50",
      "lastPage": "ul.pagination li:has(span.item-active) ~ li",
      "cookies": {
        "lang": "en"
      }
    },
    {
      "orientation": "gay",
      "link": "http://www.yeptube.com/videos/%s",
      "start_index": "1",
      "end_index": "1",
      "lastPage": "ul.pagination li:has(span.item-active) ~ li",
      "cookies": {
        "cattype": "gay",
        "lang": "en"
      }
    },
    {
      "orientation": "transsexual",
      "link": "http://www.yeptube.com/videos/%s",
      "start_index": "1",
      "end_index": "1",
      "lastPage": "ul.pagination li:has(span.item-active) ~ li",
      "cookies": {
        "cattype": "trans",
        "lang": "en"
      }
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "10"
  },
  "videos": {
    "classifier": "div.thrcol.mt10.aft div.container div.videos div.th-wr-video ins a.cover",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "video/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "//(.+?)/media/videos/tmb/(\\d+)/(\\d+)_(\\d+)/.*$",
              "replacement": "//$1/media/videos/tmb/$2/player/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".+?/media/videos/tmb/\\d+/\\d+_\\d+/(\\d+).*$",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.time",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.hd"
      }
    ]
  },
  "video_details": {
    "classifier": "div.thr-other.ohidden div.container",
    "info": [
      {
        "videoInfoType": "title",
        "classifier": "h2.c-mt-output",
        "place": "text"
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.tr-cat-wrcontent a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.tr-sponsor-cover a img",
        "place": "attr",
        "place_name": "alt"
      }
    ]
  }
}