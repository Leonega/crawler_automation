{
  "name": "babesandstars.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 100,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://www.babesandstars.com/galleries/%s/",
      "start_index": "1",
      "end_index": "5"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "36000"
  },
  "videos": {
    "classifier": "div.galleries div.item div.thumb",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "a",
        "place": "attr",
        "place_name": "href",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "^.*/.+/(.+/.+)/$",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "paysite",
        "multiple": "false",
        "classifier": "span.site",
        "place": "text"
      },
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      }
    ]
  },
  "video_details": {
    "classifier": "div.gallery div.wrap",
    "info": [
      {
        "videoInfoType": "title",
        "classifier": "h1",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "classifier": "div.pull-right div.description",
        "place": "text"
      },
      {
        "videoInfoType": "created_date",
        "classifier": "div.pull-right div.published span",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.model div.name",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.tags div.list span a",
        "place": "text",
        "filters": [
          {
            "filter": "replace_if_exist",
            "params": {
              "substring": "More...",
              "value": ""
            }
          }
        ]
      },
      {
        "videoInfoType": "thumbs",
        "multiple": "true",
        "classifier": "div.my_gallery figure a",
        "place": "attr",
        "place_name": "abs:href"
      }
    ]
  }
}