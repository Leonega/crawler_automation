{
  "name": "tubeon.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 88,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://www.tubeon.com/videos/%s",
      "start_index": "1",
      "end_index": "50",
      "lastPage": "li:has(span.clause-active) ~ li",
      "cookies": {
        "lang": "en"
      }
    },
    {
      "orientation": "gay",
      "link": "http://www.tubeon.com/videos/%s",
      "start_index": "1",
      "end_index": "1",
      "lastPage": "li:has(span.clause-active) ~ li",
      "cookies": {
        "cattype": "gay",
        "lang": "en"
      }
    },
    {
      "orientation": "transsexual",
      "link": "http://www.tubeon.com/videos/%s",
      "start_index": "1",
      "end_index": "1",
      "lastPage": "li:has(span.clause-active) ~ li",
      "cookies": {
        "cattype": "trans",
        "lang": "en"
      }
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "5"
  },
  "videos": {
    "classifier": "div.thrcol.mt15.afsite div.thr-ot.indentleft div.contain div.videos div.th-v ins a.cover",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "title",
        "classifier": "img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "video/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "(.+?)/media/videos/tmb/(\\d+)/(\\d+)_(\\d+)/.*$",
              "replacement": "$1/media/videos/tmb/$2/player/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/media/videos/tmb/\\d+/\\d+_\\d+/(\\d+).*$",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.time-desc",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.hd"
      }
    ]
  },
  "video_details": {
    "classifier": "div.contain div.wrtrailer.covertrailer",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.it-cat-covercontent a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.it-sponsor-site-cover a img",
        "place": "attr",
        "place_name": "alt"
      }
    ]
  }
}