{
  "name": "indianpornvideos",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "straight",
      "link": "https://www.indianpornvideos2.com/page/%s/",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "30",
    "count": "10"
  },
  "videos": {
    "classifier": " main#main ",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a.video_thumb_link",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a.video_thumb_link img",
        "place": "attr",
        "place_name": "abs:src"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.pull-right",
        "place": "text"
      },
      {
        "videoInfoType": "title",
        "classifier": "a.video_thumb_link img",
        "place": "attr",
        "place_name": "alt"
      }
    ]
  },
  "video_details": {
    "classifier": "main#main",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.tags-links a",
        "place": "text"
      },
      {
        "videoInfoType": "id",
        "classifier": "article",
        "place": "attr",
        "place_name": "id",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "post-"
            }
          }
        ]
      },
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.cat-links a",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "multiple": "true",
        "classifier": "div.tags-links a:contains(hd)",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.entry-content.col-sm-12.col-md-12 > p",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.indianpornvideos2.com/embed/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}