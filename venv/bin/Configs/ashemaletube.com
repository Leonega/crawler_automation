{
  "name": "ashemaletube.com",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "transsexual",
      "link": "https://www.ashemaletube.com/videos/newest/%s/",
      "start_index": "1",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "15",
    "count": "5"
  },
  "videos": {
    "classifier": "li.thumb-item.videospot div.thumb.vidItem",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "data-video-id"
      },
      {
        "videoInfoType": "title",
        "classifier": "a img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "/ast-\\d+x\\d+/(\\d+-\\d+)/(.+)-\\d+x\\d+-\\d+.jpg",
              "replacement": "/ast-full/$1/$2-full-%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": "https://.+?/thumbs/.+?-320x240/\\d+-\\d+/.+?/.+?.mp4-320x240-(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.text-active.bold",
        "place": "text"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.fs11.viddata.flr:not(span.text-active.bold)",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div.bottom-info-box",
    "info": [
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "a.model-card",
        "place": "text"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.content-tags a.tag-item",
        "place": "text"
      }
    ]
  }
}