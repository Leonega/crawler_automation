{
  "name": "xvideos.com",
  "botCheckMs": "1000",
  "stopNumberExistingVideosInRow": 10,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "https://www.xvideos.com/new/%s",
      "start_index": "1",
      "end_index": "1"
    },
    {
      "orientation": "gay",
      "link": "https://www.xvideos.com/gay/%s",
      "start_index": "0",
      "end_index": "1"
    },
    {
      "orientation": "transsexual",
      "link": "https://www.xvideos.com/shemale/%s",
      "start_index": "0",
      "end_index": "1"
    }
  ],
  "scheduler": {
    "intervalInSeconds": "600"
  },
  "thumbs": {
    "start": "1",
    "end": "30",
    "count": "10"
  },
  "videos": {
    "classifier": "div#main div.thumb-block",
    "info": [
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "id",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "video_"
            }
          }
        ]
      },
      {
        "videoInfoType": "link",
        "classifier": "a",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "div.thumb a img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "/videos/.+?/(.+/.+?.)\\d+.jpg",
              "replacement": "/videos/thumbs169lll/$1%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "div.thumb a img",
        "place": "attr",
        "place_name": "abs:data-src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": "https://.+?/.+?/.+?/.+?/.+?/.+?/.+?/.+?\\.(.+?).jpg",
              "replacement": "$1"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "p a",
        "place": "attr",
        "place_name": "title"
      },
      {
        "videoInfoType": "runtime",
        "classifier": "span.duration",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "span.video-hd-mark",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#main",
    "info": [
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "ul li a.btn.btn-default",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "ul li a.btn.btn-default.label.profile.hover-name span.name",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "ul li a.btn.btn-default.label.main.uploader-tag.hover-name span.name",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://www.xvideos.com/embedframe/%s\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}