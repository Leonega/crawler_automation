{
  "name": "hotmovs.com",
  "botCheckMs": "100",
  "stopNumberExistingVideosInRow": 70,
  "traversal": [
    {
      "orientation": "unknown",
      "link": "http://hotmovs.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "16000",
      "lastPage": "nav#list_videos2_common_videos_list_pagination ul li.active~li"
    },
    {
      "orientation": "gay",
      "link": "http://hotmovs.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "1500",
      "lastPage": "nav#list_videos2_common_videos_list_pagination ul li.active~li",
      "cookies": {
        "category_group_id": "67"
      }
    },
    {
      "orientation": "transsexual",
      "link": "http://hotmovs.com/latest-updates/%s/",
      "start_index": "1",
      "end_index": "1200",
      "lastPage": "nav#list_videos2_common_videos_list_pagination ul li.active~li",
      "cookies": {
        "category_group_id": "68"
      }
    }
  ],
  "scheduler": {
    "intervalInSeconds": "3600"
  },
  "thumbs": {
    "start": "1",
    "end": "20",
    "count": "5"
  },
  "videos": {
    "classifier": "div#list_videos2_common_videos_list_items article.item a",
    "info": [
      {
        "videoInfoType": "link",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href"
      },
      {
        "videoInfoType": "id",
        "classifier": "",
        "place": "attr",
        "place_name": "abs:href",
        "filters": [
          {
            "filter": "trim_before",
            "params": {
              "substring": "videos/"
            }
          },
          {
            "filter": "trim_after",
            "params": {
              "substring": "/"
            }
          }
        ]
      },
      {
        "videoInfoType": "title",
        "classifier": "img",
        "place": "attr",
        "place_name": "alt"
      },
      {
        "videoInfoType": "thumb",
        "classifier": "img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "replace_rexegp",
            "params": {
              "regexp": ".*contents/videos_screenshots/(\\d+/\\d+)/(\\d+x\\d+)/\\d+.jpg",
              "replacement": "http://direct.hotmovs.com/contents/videos_sources/$1/screenshots/%s.jpg"
            }
          }
        ]
      },
      {
        "videoInfoType": "main_thumb",
        "classifier": "a img",
        "place": "attr",
        "place_name": "abs:src",
        "filters": [
          {
            "filter": "regexp",
            "params": {
              "regexp": ".+?/contents/videos_screenshots/\\d+/\\d+/\\d+x\\d+/(\\d+).jpg",
              "group": "1"
            }
          }
        ]
      },
      {
        "videoInfoType": "runtime",
        "classifier": "div.thumbnail__info__right",
        "place": "text"
      }
    ]
  },
  "video_details": {
    "classifier": "div#holder",
    "info": [
      {
        "videoInfoType": "categories",
        "multiple": "true",
        "classifier": "div.info div a.label-default[title=Predefined category]",
        "place": "text"
      },
      {
        "videoInfoType": "hd",
        "classifier": "div.info div a.label-default[title=Predefined category]:containsOwn(HD)"
      },
      {
        "videoInfoType": "tags",
        "multiple": "true",
        "classifier": "div.info div a.label-default[title=Predefined tag]",
        "place": "text"
      },
      {
        "videoInfoType": "models",
        "multiple": "true",
        "classifier": "div.info div a.label-default[title=Predefined model]",
        "place": "text"
      },
      {
        "videoInfoType": "description",
        "multiple": "true",
        "classifier": "div.item:nth-child(2) span em",
        "place": "text"
      },
      {
        "videoInfoType": "paysite",
        "classifier": "div.info ul.nav.nav-tabs.nav-info li a[aria-controls=channel_videos]",
        "place": "text"
      }
    ]
  },
  "embedded_template": "<iframe src=\"https://hotmovs.com/embed/%s/\" frameborder=\"0\" height=\"640\" width=\"480\" scrolling=\"no\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>"
}