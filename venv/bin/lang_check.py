from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import StaleElementReferenceException, NoSuchElementException
import re, os, sys, time


driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
list_dom = []
list_id = []
list_colors = []
list_header1_before = []
list_header2_before = []
list_header1_after = []
list_header2_after = []

def cj_log():
    # driver.set_window_position(1910, 0)
    driver.maximize_window()
    driver.get('http://deployer.com/cj_installer/admin/login')
    driver.implicitly_wait(10)
    cj_login_input = driver.find_element_by_xpath('//*[@id="username"]')
    cj_login_input.send_keys('логин')
    cj_password_input = driver.find_element_by_xpath('//*[@id="password"]')
    cj_password_input.send_keys('пароль')
    cj_press_button = driver.find_element_by_xpath('/html/body/div/div[2]/form/div[3]/div[2]/button')
    cj_press_button.click()

def sites_select_pagg():
    sites_button = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/a')
    sites_button.click()
    show_not = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/ul/li[2]/a')
    show_not.click()

    pagging = driver.find_elements_by_css_selector('#DataTables_Table_0_paginate > ul li a')
    pag = int(pagging[-2].text)  # номер последней страницы паггинации
    for page in range(1, pag + 1):
        collect_domain_name = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(2)')
        collect_id_number = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(1)')
        for dom in collect_domain_name:
            list_dom.append(dom.text)
        for num in collect_id_number:
            list_id.append(num.text)
        page_button = driver.find_element_by_css_selector('#DataTables_Table_0_next > a')
        page_button.click()
        driver.implicitly_wait(10)
        time.sleep(4)


def lang_check():
    for site in list_dom:
        index = list_dom.index(site)
        a = r'http://deployer.com/cj_installer/admin/sites/' + list_id[index] + r'/edit'
        driver.get(a)
        lang_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/form/div[1]/ul/li[3]/a')
        lang_button.click()
        lang_select_field = driver.find_element_by_xpath('//*[@id="language_private"]').get_attribute('value')
        print(str(index+1)+') ' + site +', language - ' + lang_select_field)
        templet_gene_button = driver.find_element_by_css_selector('body > div > div > section.content > form > div:nth-child(3) > ul > li:nth-child(4) > a')
        templet_gene_button.click()
        header_before = driver.find_elements_by_css_selector('#tree-root > li:nth-child(1) > div.ng-scope.angular-ui-tree.header > ol li.ng-scope.angular-ui-tree-node div.ng-scope.ng-binding.angular-ui-tree-handle')
        for h in header_before:
            list_header1_before.append(h.get_attribute('outerText'))
        for lhb in list_header1_before:
            lh = re.sub(r'\n×', '', lhb)  # >> × << не x
            list_header2_before.append(lh)
        if ('languages' and 'meta_alternative') not in list_header2_before:
            print("OK, 'languages' and 'meta_alternative' not in Template" + '\n')
        else:
            apply_this_totree_button = driver.find_element_by_css_selector('#fbc484b8e67c12cb1f58d60ba01c2d56 > div > div:nth-child(2) > div > div:nth-child(2) > div > a:nth-child(2)')
            apply_this_totree_button.click()
            time.sleep(1)
            header_after = driver.find_elements_by_css_selector('#tree-root > li:nth-child(1) > div.ng-scope.angular-ui-tree.header > ol li.ng-scope.angular-ui-tree-node div.ng-scope.ng-binding.angular-ui-tree-handle')
            for h in header_after:
                list_header1_after.append(h.get_attribute('outerText')) 
            for color in header_after:
                list_colors.append(color.value_of_css_property('background-color'))
            # if list_colors[met_alter_numb] == 'rgba(240, 139, 0, 0.17)':
            #     print('Oh Yeah')
            for lhb in list_header1_after:
                lh = re.sub(r'\n×', '', lhb)  # >> × << не x
                list_header2_after.append(lh)
            if 'meta_alternative' in list_header2_after:
                try:
                    met_alter_numb = list_header2_after.index('meta_alternative')
                    if lang_select_field == 'english' and (list_colors[met_alter_numb] == 'rgba(240, 139, 0, 0.27)'):
                        print('No. "meta_alternative" in green color' + '\n')
                except NoSuchElementException:
                    pass
            if lang_select_field != 'english' and ('meta_alternative' and 'languages' in list_header2_after):
                print('OK. Language Not ENGLISH' + '\n')
            elif lang_select_field == 'english' and (('languages' and 'meta_alternative') not in list_header2_after):
                print('OK. Language Selected As ENGLISH' + '\n')
            # elif lang_select_field != 'english' and ('languages' not in list_header2_after):
            #     print('No. "languages" is not selected' + '\n')
            elif lang_select_field != 'english' and ('meta_alternative' not in list_header2_after):
                print('No. "meta_alternative" is not selected' + '\n')
            # elif lang_select_field == 'english' and ('meta_alternative' in list_header2_after):
            #     print('No. "meta_alternative" is selected' + '\n')
            # try:
            # elif lang_select_field == 'english' and (list_colors[met_alter_numb] != 'rgba(240, 139, 0, 0.17)'):
            #     print('No. "meta_alternative" in green color' + '\n')
            # except:
            #     pass
            elif lang_select_field != 'english' and ('languages' not in list_header2_before) and ('meta_alternative' in list_header2_after):
                print("OK Language Not ENGLISH, 'languages' not in Template" + '\n')
            # elif lang_select_field == 'english' and (('languages' or 'meta_alternative') in list_header2_after):
            #     print('No. "languages" or "meta_alternative" is selected' + '\n')
            else:
                print('OK. Language Selected As ENGLISH'+ '\n')
            list_header1_before.clear()
            list_header2_before.clear()
            list_header1_after.clear()
            list_header2_after.clear()

def main():
    cj_log()
    sites_select_pagg()
    lang_check()
    driver.quit()

if __name__ == '__main__':
    main()