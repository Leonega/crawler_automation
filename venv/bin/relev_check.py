from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import re, os, sys, time


driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
list_dom = []
list_id = []
list_footer1_before = []
list_footer2_before = []
list_footer1_after = []
list_footer2_after = []

def cj_log():
    driver.set_window_position(1910, 0)
    driver.maximize_window()
    driver.get('http://deployer.com/cj_installer/admin/login')
    driver.implicitly_wait(10)
    cj_login_input = driver.find_element_by_xpath('//*[@id="username"]')
    cj_login_input.send_keys('логин')
    cj_password_input = driver.find_element_by_xpath('//*[@id="password"]')
    cj_password_input.send_keys('пароль')
    cj_press_button = driver.find_element_by_xpath('/html/body/div/div[2]/form/div[3]/div[2]/button')
    cj_press_button.click()

def sites_select_pagg():
    sites_button = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/a')
    sites_button.click()
    show_not = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/ul/li[2]/a')
    show_not.click()

    pagging = driver.find_elements_by_css_selector('#DataTables_Table_0_paginate > ul li a')
    pag = int(pagging[-2].text)  # номер последней страницы паггинации
    for page in range(1, pag + 1):
        collect_domain_name = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(2)')
        collect_id_number = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(1)')
        for dom in collect_domain_name:
            list_dom.append(dom.text)
        for num in collect_id_number:
            list_id.append(num.text)
        page_button = driver.find_element_by_css_selector('#DataTables_Table_0_next > a')
        page_button.click()
        driver.implicitly_wait(10)
        time.sleep(4)

def go_to_template():
        for site in list_dom:
            index = list_dom.index(site)
            a = r'http://deployer.com/cj_installer/admin/sites/' + list_id[index] + r'/edit'
            driver.get(a)
            private_sett_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/form/div[1]/ul/li[2]/a')
            private_sett_button.click()
            search_base_name = driver.find_element_by_xpath('//*[@id="search_base_name"]')
            driver.implicitly_wait(5)
            print(str(index+1)+') ' + site + ' ' + search_base_name.get_attribute('value'))
            templet_gene_button = driver.find_element_by_css_selector('body > div > div > section.content > form > div:nth-child(3) > ul > li:nth-child(4) > a')
            templet_gene_button.click()
            templet_footer_before = driver.find_elements_by_css_selector('div.ng-scope.angular-ui-tree.footer li.ng-scope.angular-ui-tree-node div.ng-scope.angular-ui-tree.blocks li div.ng-scope.ng-binding.angular-ui-tree-handle')
            for elemb in templet_footer_before[1:]:
                list_footer1_before.append(elemb.get_attribute('outerText'))
            # print(list_footer1_before)
            for lfa in list_footer1_before:
                la = re.sub(r'\n×', '', lfa)  # >> × << не x
                list_footer2_before.append(la)
            if ('relevant_searches' and 'related_search') not in list_footer2_before:
                print("OK, 'relevant_searches' and 'related_search' not in Template" + '\n')
            else:
                apply_this_totree_button = driver.find_element_by_css_selector('#fbc484b8e67c12cb1f58d60ba01c2d56 > div > div:nth-child(2) > div > div:nth-child(2) > div > a:nth-child(2)')
                apply_this_totree_button.click()
                time.sleep(1)
                templet_footer_after = driver.find_elements_by_css_selector('div.ng-scope.angular-ui-tree.footer li.ng-scope.angular-ui-tree-node div.ng-scope.angular-ui-tree.blocks li div.ng-scope.ng-binding.angular-ui-tree-handle')
                # templet_footer_after = driver.find_elements_by_css_selector('div.ng-scope.angular-ui-tree')
                for temp in templet_footer_after[1:]:
                    list_footer1_after.append(temp.get_attribute('outerText'))
                for lf in list_footer1_after:
                    l = re.sub(r'\n×','',lf)     # >> × << не x
                    list_footer2_after.append(l)
                if search_base_name.get_attribute('value') != '' and ('relevant_searches' and 'related_search' in list_footer2_after):
                    print("OK" + '\n')
                elif search_base_name.get_attribute('value') != '' and ('relevant_searches' and 'related_search' not in list_footer2_after):
                    print("OH NO -'relevant_searches' or 'related_search' не добавлены" + '\n')
                elif search_base_name.get_attribute('value') == '' and ('relevant_searches' and 'related_search' in list_footer2_after):
                    print("'relevant_searches','related_search' added,while search_base_name - empty" + '\n')
                elif search_base_name.get_attribute('value') == '' and ('relevant_searches' and 'related_search' not in list_footer2_after):
                    print("OK - 'relevant_searches','related_search' не добавлены" + '\n')
                list_footer1_before.clear()
                list_footer2_before.clear()
                list_footer1_after.clear()
                list_footer2_after.clear()

def main():
    cj_log()
    sites_select_pagg()
    go_to_template()
    driver.quit()

if __name__ == '__main__':
    main()