

class Programmist():

    def __init__(self, name, age, level):
        self.name = name
        self.age = age
        self.level = level

    def entrance_work(self):
        if self.level == 0:
            print(self.name + ' Проспал')
        else:
            print(self.name + ' Вовремя')


roman = Programmist('Roman', 36, 1)
dmitrii = Programmist('Dmitrii', 30, 0)

roman.entrance_work()
dmitrii.entrance_work()