from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import openpyxl
import re, os,sys
from openpyxl import load_workbook
import time

# chrome_driver_path = '/Users/testguru/Desktop/Excell/chromedriver'
list_of_lists = []
list_batch = []
task_list_todo = []
digit_list_todo = []
links_list_todo = []

task_list_inprogress = []
digit_list_inprogress = []
links_list_inprogress = []

task_list_done = []
digit_list_done = []
links_list_done = []

list_dom=[]
list_id=[]
list_column= []
list_row= []
list_domain_keyword= []
list_preset=[]

webPage = input(
    'Введите URL Задачи, пример -> https://jira.89team.online/browse/MS-2161 и нажмите Enter : ' + '\n').rstrip().lstrip()
taska = re.sub(r'https://jira.89team.online/browse/', '', webPage)


def data_collection():
    global webPage,taska

    while webPage == '':
        print('Вы ввели пустое поле')
        webPage = input('Введите URL Задачи, пример -> https://jira.89team.online/browse/MS-2161 и нажмите Enter : ' + '\n').rstrip().lstrip()
        taska = re.sub(r'https://jira.89team.online/browse/', '', webPage)
    driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
    # driver = webdriver.Chrome(executable_path=("%s" % chrome_driver_path))
    driver.maximize_window()
    driver.get('https://jira.89team.online/secure/RapidBoard.jspa?rapidView=9&view=detail&quickFilter=17')
    # driver.get('https://jira.89team.online/secure/RapidBoard.jspa?rapidView=9&view=detail&selectedIssue=MS-2155&quickFilter=17')
    jira_login = driver.find_element_by_xpath('//*[@id="login-form-username"]')
    jira_login.send_keys('логин')
                    # print('Введеите Логин в Jira, нажмите Enter ')
                    # login_jira_input = input('')
                    # jira_login.send_keys(login_jira_input)
    jira_password = driver.find_element_by_xpath('//*[@id="login-form-password"]')
    jira_password.send_keys('пароль')
                    # print('Введеите Пароль в Jira, нажмите Enter ')
                    # password_jira_input = input('')
                    # jira_password.send_keys(password_jira_input)
    jira_submmit = driver.find_element_by_xpath('//*[@id="login-form-submit"]')
    jira_submmit.click()
    driver.implicitly_wait(30)
    tasks = driver.find_elements_by_css_selector('#ghx-pool > div > ul.ghx-columns li.ghx-column.ui-sortable')
    task_pattern = re.compile(r'^(\w\w-\d+)')
    for task in tasks[::3]:
        task_list_todo.append(str(task.get_attribute('textContent')))
    for sort_task in task_list_todo:
        if sort_task != '':
            s = re.split(task_pattern, sort_task)
            digit_list_todo.append(int(re.sub(r'\D', '', s[1])) - 1)
    for digit in digit_list_todo:
        links_list_todo.append(r'https://jira.89team.online/browse/MS-' + str(digit))

    for task in tasks[1::3]:
        task_list_inprogress.append(str(task.get_attribute('textContent')))
    for sort_task in task_list_inprogress:
        if sort_task != '':
            s = re.split(task_pattern, sort_task)
            digit_list_inprogress.append(int(re.sub(r'\D', '', s[1])) - 1)
    for digit in digit_list_inprogress:
        links_list_inprogress.append(r'https://jira.89team.online/browse/MS-' + str(digit))

    for task in tasks[2::3]:
        task_list_done.append(str(task.get_attribute('textContent')))
    for sort_task in task_list_done:
        if sort_task != '':
            s = re.split(task_pattern, sort_task)
            digit_list_done.append(int(re.sub(r'\D', '', s[1])) - 1)
    for digit in digit_list_done:
        links_list_done.append(r'https://jira.89team.online/browse/MS-' + str(digit))


    while webPage not in links_list_todo:
        if webPage in links_list_done:
            print('Задача ' + taska + " Уже Выполнена")
            print('Повторить Ввод Заново? y/n')
            answ = input()
            if answ == 'y':
                webPage = input(
                    'Введите URL Задачи, пример -> https://jira.89team.online/browse/MS-2161 и нажмите Enter : ' + '\n').rstrip().lstrip()
                taska = re.sub(r'https://jira.89team.online/browse/', '', webPage)
            elif answ == 'n':
                print('Запустите программу сначала!')
                driver.quit()
                sys.exit()
            else:
                print('Вы ввели не "y" и не "n"')
                print('Запустите программу сначала!')
                driver.quit()
                sys.exit()
        elif webPage in links_list_inprogress:
            while webPage in links_list_inprogress:
                print('Задача ' + taska + " Уже Выполняется")
                print('Повторить Ввод Заново? y/n')
                answ = input()
                if answ == 'y':
                    webPage = input('Введите URL Задачи, пример -> https://jira.89team.online/browse/MS-2161 и нажмите Enter : ' + '\n').rstrip().lstrip()
                    taska = re.sub(r'https://jira.89team.online/browse/', '', webPage)
                elif answ == 'n':
                    print('Запустите программу сначала!')
                    driver.quit()
                    sys.exit()
                else:
                    print('Вы ввели не "y" и не "n"')
                    print('Запустите программу сначала!')
                    driver.quit()
                    sys.exit()
        elif webPage == '':
            print('Вы ввели пустое поле')
            print('Повторить Ввод Заново? y/n')
            answ = input()
            if answ == 'y':
                webPage = input(
                    'Введите URL Задачи, пример -> https://jira.89team.online/browse/MS-2161 и нажмите Enter : ' + '\n').rstrip().lstrip()
                taska = re.sub(r'https://jira.89team.online/browse/', '', webPage)
            elif answ == 'n':
                print('Запустите программу сначала!')
                driver.quit()
                sys.exit()
            else:
                print('Вы ввели не "y" и не "n"')
                print('Запустите программу сначала!')
                driver.quit()
                sys.exit()
        else:
            print(webPage + ' <-- Такой Задачи Не Существует')
            print('Повторить Ввод Заново? y/n')
            answ = input()
            if answ == 'y':
                webPage = input(
                    'Введите URL Задачи, пример -> https://jira.89team.online/browse/MS-2161 и нажмите Enter : ' + '\n').rstrip().lstrip()
                taska = re.sub(r'https://jira.89team.online/browse/', '', webPage)
            elif answ == 'n':
                print('Запустите программу сначала!')
                driver.quit()
                sys.exit()
            else:
                print('Вы ввели не "y" и не "n"')
                print('Запустите программу сначала!')
                driver.quit()
                sys.exit()
    print('Вы выбрали Задачу ' + taska)
    driver.get(webPage)
    preset_field = driver.find_element_by_css_selector('#description-val > div > p:nth-child(1)').get_attribute('textContent')
    result = re.split(r'\n', preset_field )
    preset = driver.find_element_by_xpath('//*[@id="summary-val"]').text
    server = re.sub(r'Server: ', '', result[1])
    ftt_serv =re.sub(r'Ftt Server: Tube ', '', result[2])
    ftt_server = re.findall(r'[(].+[)]', ftt_serv)
    cdn_thumb_serv = re.sub(r'CDN Thumb Server: ', '', result[3])
    cdn_thumb_server = re.findall(r'-\s.+', cdn_thumb_serv)
    feed_main_cat = re.sub(r'feed_main_category_id: ', '', result[5])
    batch = driver.find_elements_by_css_selector('#description-val > div > p')
    for p in batch[3:9]:
        list_batch.append(p.text)

    excel_table = driver.find_element_by_css_selector('#description-val > div > p a')
    excel_table.click()
    time.sleep(3)
    save_to_my = driver.find_element_by_css_selector('#TB_FILE_IDENTITY > span')
    save_to_my.click()
                            # zoho_login = driver.find_element_by_css_selector('#block-system-main > div > div.header-part > span > a')
                            # zoho_login.click()
                            # sign_in = driver.find_element_by_css_selector('#openidcontainer > div.fs_signin_options_txt')
                            # sign_in.click()
                            # time.sleep(3)
                            # google = driver.find_element_by_css_selector('#fs-google-icon')
                            # google.click()
                            # input_log = driver.find_element_by_css_selector('#identifierId')
                            # input_log.send_keys(r'емейл')
                            # next_but = driver.find_element_by_css_selector('#identifierNext > span > span')
                            # next_but.click()
                            # input_pass = driver.find_element_by_css_selector('#password > div.aCsJod.oJeWuf > div > div.Xb9hP > input')
                            # input_pass.send_keys('пароль' + Keys.ENTER)
                            # time.sleep(2)
                            # file_tab = driver.find_element_by_css_selector('#filetw > a')
                            # file_tab.click()
                            # time.sleep(2)
    dowhload_how = driver.find_element_by_css_selector('#MI_8_MENU > span')
    dowhload_how.click()
    time.sleep(2)
    save_format = driver.find_element_by_css_selector('#SMI_8_IDENTITY > li:nth-child(1)')
    time.sleep(2)
    save_format.click()
    time.sleep(5)

    driver.get('http://deployer.com/cj_installer/admin/login')
    driver.maximize_window()
    driver.implicitly_wait(10)
    cj_login_input = driver.find_element_by_xpath('//*[@id="username"]')
    cj_login_input.send_keys('логин')
    cj_password_input = driver.find_element_by_xpath('//*[@id="password"]')
    cj_password_input.send_keys('пароль')
    cj_Press_Button = driver.find_element_by_xpath('/html/body/div/div[2]/form/div[3]/div[2]/button')
    cj_Press_Button.click()

    for n in range(len(list_batch)):
            driver.get('http://deployer.com/cj_installer/admin/login')
            sites_button = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/a')
            sites_button.click()
            add_batch_sites = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/ul/li[4]/a')
            add_batch_sites.click()

            preset_field = driver.find_element_by_xpath('//*[@id="select2-preset_id-container"]')
            preset_field.click()
            preset_input = driver.find_element_by_xpath('/html/body/span/span/span[1]/input')
            preset_input.send_keys(preset + Keys.ENTER)

            server_button = driver.find_elements_by_css_selector('#server_id option')
            for server_cj in server_button:
                if server_cj.text == server:
                    server_cj.click()

            ftt_server_button = driver.find_elements_by_css_selector('#ftt_server_id option')
            for ftt_server_cj in ftt_server_button:
                if ftt_server[0] in ftt_server_cj.text:
                    ftt_server_cj.click()

            cdn_thumb_button = driver.find_elements_by_css_selector('#cdn_thumb_server_id option')
            for cdn_thumb_cj in cdn_thumb_button:
                if cdn_thumb_server[0] in cdn_thumb_cj.text:
                    cdn_thumb_cj.click()

            for text in list_batch[n:]:
                length = len(list_batch) - n
                text_area = driver.find_element_by_css_selector('#batch')
                text_area.send_keys(text + '\n\n')
                # print(text + str(type(text)) +' '+ str(n))
                # print('\t')
                if n == (len(list_batch)-1):
                    print(r'Сайты\Сайт Из Задачи ' + taska + r' Уже Были Добалены Ранее Или Задача Не Переведена Из "To-DO" --> "In Progress", Проверить Входные\Выходные Данные')
                    # driver.quit()
                    # driver.close()
                    break
                    driver.quit()
            add_batch_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/section/form/div/div[8]/div/input')
            add_batch_button.click()
            time.sleep(2)
            if driver.current_url == 'http://deployer.com/cj_installer/admin/sites/batch' and n < int(len(list_batch) - 1):
                n += 1
            else:
                break
    # pagging = driver.find_elements_by_css_selector('#DataTables_Table_0_paginate > ul li a')
    #
    # filenames = os.listdir('/Users/testguru/Downloads')
    # for f in filenames:
    #     if f.startswith('Копия domains' or 'domains' or 'Copy of domains') and f.endswith('.xlsx'):
    #         p = '/Users/testguru/Downloads/' + f
    # time.sleep(2)
    # wb = load_workbook(p)
    # sheet = wb['Лист1']
    # sheet = wb.active
    # for row in sheet['A1':'A50']:
    #     for cell in row:
    #         list_column.append(cell.value)
    # for row in sheet['E1':'E50']:
    #     for cell in row:
    #         list_row.append(cell.value)
    # for el in range(len(list_column)):
    #     list_domain_keyword.append(str(list_column[el]) + " " + str(list_row[el]))
    #
    # for page in pagging[1:-1]:
    #     collect_domain_name = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(2)')
    #     collect_id_number = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(1)')
    #     current_preset = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(3)')
    #     for dom in collect_domain_name:
    #         list_dom.append(dom.text)
    #     for num in collect_id_number:
    #         list_id.append(num.text)
    #     for cur in current_preset:
    #         list_preset.append(cur.text)
    #     page_button = driver.find_element_by_css_selector('#DataTables_Table_0_next > a')
    #     page_button.click()
    #     time.sleep(2)
    #
    # for pr in range(len(list_preset)):
    #     if preset == list_preset[pr]:
    #         for site in list_dom:
    #                     if site == list_dom[pr]:
    #                         index = list_dom.index(site)
    #                         column_index = list_column.index(site)
    #                         print(site + ' ID - ' + list_id[index] + ' KEYWORDS - ' + str(list_row[column_index]))
    #                         a = r'http://deployer.com/cj_installer/admin/sites/' + list_id[index] + r'/edit'
    #                         driver.get(a)
    #                         private_sett_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/form/div[1]/ul/li[2]/a')
    #                         private_sett_button.click()
    #                         feed_main_cat_id_field = driver.find_element_by_css_selector('#feed_main_category_id')
    #                         search_base_name = driver.find_element_by_xpath('//*[@id="search_base_name"]')
    #                         if str(list_row[column_index]) == 'None':
    #                             search_base_name.clear()
    #                         else:
    #                             search_base_name.clear()
    #                             search_base_name.send_keys(str(list_row[column_index]))
    #                         if feed_main_cat != 0:
    #                             feed_main_cat_id_field.clear()
    #                             feed_main_cat_id_field.send_keys(feed_main_cat)
    #                         save_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/form/div[2]/input')
    #                         save_button.click()
    # os.remove(p)
    if 'Sites' in driver.title:
        print('Пачка из ' + str(length) + ' сайтов успешно добавлена')
    driver.quit()
# data_collection()

if __name__  == '__main__':
    data_collection()
