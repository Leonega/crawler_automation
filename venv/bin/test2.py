from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
import time
import re

chrome_driver_path = '/Users/testguru/Downloads/chromedriver 4'
username = 'username'
password = 'password'
base_url = 'localhost:8080/'  # ie 127.0.0.1:8080,
baseUrl = 'http://%s:%s@%s' % (username, password, base_url)
driver = webdriver.Chrome(executable_path=("%s" % chrome_driver_path))
driver.get(baseUrl)
driver.maximize_window()


path_for_crawlers_preview = '/Users/testguru/Desktop/crawlers_preview_format'
preview_buttons = driver.find_elements_by_css_selector('#tubesTable > tbody > tr> td:nth-child(3) > a')
cat_list = []

if not os.path.exists(path_for_crawlers_preview):
    os.mkdir(path_for_crawlers_preview)

def crawlers_preview():
    for i in range(len(preview_buttons)):
        file_name = driver.find_elements_by_css_selector('#tubesTable > tbody > tr > td:nth-child(2)')[i]
        path = path_for_crawlers_preview + '/' + file_name.text
        a = driver.find_elements_by_css_selector('#tubesTable > tbody > tr> td:nth-child(3) > a')[i]
        a.click()
        orientation = driver.find_elements_by_css_selector('body > div.container > div > div:nth-child(1) > h2')
        if not os.path.isfile(path):
            with open(path, 'w') as f:
                try:
                    for o in range(len(orientation)):
                        orientation_name = driver.find_elements_by_css_selector('body > div.container > div > div:nth-child(1) > h2')[o]
                        id_value = driver.find_elements_by_css_selector('body > div.container > div > div > table > tbody > tr:nth-child(2) > td:nth-child(1)')[o]
                        link_value = driver.find_elements_by_css_selector('.container > div > div > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2)')[o]
                        thumb_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(3)')[o]
                        main_thumb_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(4)')[o]
                        video_preview = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(5)')[o]
                        hd_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(6)')[o]
                        duration_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(7)')[o]
                        title_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(8)')[o]
                        description_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(9)')[o]
                        paysite_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(10)')[o]
                        created_date_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(11)')[o]
                        categories_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(13)')[o]
                        cat = re.sub(r'\n',',', categories_value.text)
                        tags_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(14)')[o]
                        tag = re.sub(r'\n',',', tags_value.text)
                        model_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(15)')[o]
                        model = re.sub(r'\n',',', model_value.text)
                        created_date_parsed_value = driver.find_elements_by_css_selector('.container > div > div:nth-child(1) > table:nth-child(2) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(16)')[o]
                        f.write(orientation_name.get_attribute('textContent') + '\n')
                        f.write('-----------------------------------\n')
                        f.write('ID = ' + id_value.get_attribute('textContent') + '\n')
                        f.write('Link = ' + link_value.get_attribute('textContent') + '\n')
                        f.write('Thumb = ' + thumb_value.get_attribute('textContent') + '\n')
                        f.write('Main_Thumb = ' + main_thumb_value.get_attribute('textContent') + '\n')
                        f.write('Video_preview = ' + video_preview.get_attribute('textContent') + '\n')
                        f.write('HD = ' + hd_value.get_attribute('textContent') + '\n')
                        f.write('Duration = ' + duration_value.get_attribute('textContent') + '\n')
                        f.write('Title = ' + title_value.get_attribute('textContent') + '\n')
                        f.write('Description = ' + description_value.get_attribute('textContent') + '\n')
                        f.write('Paysite = ' + paysite_value.get_attribute('textContent') + '\n')
                        f.write('Created date = ' + created_date_value.get_attribute('textContent') + '\n')
                        f.write('Categories = ' + str(cat) + '\n')
                        f.write('Tags = ' + str(tag) + '\n')
                        f.write('Models = ︎' + str(model) + '\n')
                        f.write('Date_parsed = ' + created_date_parsed_value.get_attribute('textContent') + '\n')
                        f.write('\n')
                        f.write('\n')
                except:
                    continue
        back_to_crawlers_button = driver.find_element_by_xpath('/html/body/div[1]/nav/div/div/a')
        back_to_crawlers_button.click()
crawlers_preview()
driver.quit()

