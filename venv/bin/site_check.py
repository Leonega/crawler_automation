from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import StaleElementReferenceException, NoSuchElementException
import requests
import re, os, sys, time

chrome_options = Options()
chrome_options.add_argument("--headless")

driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=chrome_options)

list_dom = []
list_id = []
headers_list = []
h3_list = []
h2_list = []

path_for_sites_check = '/Users/testguru/Desktop/path_for_sites_check'

if not os.path.exists(path_for_sites_check):
    os.mkdir(path_for_sites_check)


def cj_log():
    # driver.set_window_position(1910,0)
    driver.maximize_window()
    # driver.set_window_position(1910, 0)
    driver.get('http://deployer.com/cj_installer/admin/login')
    driver.implicitly_wait(10)
    cj_login_input = driver.find_element_by_xpath('//*[@id="username"]')
    cj_login_input.send_keys('лонин')
    cj_password_input = driver.find_element_by_xpath('//*[@id="password"]')
    cj_password_input.send_keys('пароль')
    cj_press_button = driver.find_element_by_xpath('/html/body/div/div[2]/form/div[3]/div[2]/button')
    cj_press_button.click()


def sites_select_pagg():
    sites_button = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/a')
    sites_button.click()
    show_not = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/ul/li[2]/a')
    show_not.click()
    time.sleep(3)

    pagging = driver.find_elements_by_css_selector('#DataTables_Table_0_paginate > ul li a')
    pag = int(pagging[-2].text)  # номер последней страницы паггинации
    for page in range(1, pag + 1):
        collect_domain_name = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(2)')
        collect_id_number = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(1)')
        for dom in collect_domain_name:
            list_dom.append(dom.text)
        for num in collect_id_number:
            list_id.append(num.text)

        page_button = driver.find_element_by_css_selector('#DataTables_Table_0_next > a')
        page_button.click()
        driver.implicitly_wait(10)
        time.sleep(4)
    print(list_id)


def conditions(index, site, current_window, pathf, text_area):
    list_c = [item for item in list_with_a if item not in list_with_b]
    if len(list_with_b) == 0:
        list_c = list_with_a
    for h in headers:
        headers_list.append(h.get_attribute('outerText'))
    lh = [i for i in headers_list if headers_list.count(i) >= 2]
    with open(pathf, 'w') as f:
        f.write((str(index + 1) + ') ' + site) + '\n')
        f.write('Title - ' + driver.title + '\n')
        f.write(text_area)
        if len(list_c)> 0:
            f.write(str([i.get_attribute('outerText') for i in list_c]) + '\n')
        else:
            f.write('\n')
            f.write('-------------------------------------------No Headers------------------------------------------' + '\n')
        if len(lh) > 0:
            f.write(str(lh))
            f.write('\n')
            f.write('Fail' + '\n')
        else:
            f.write('OK - No Repeatable "Headers" Elements' + '\n')
        for i in list_c:
            if i.get_attribute('href') == '':
                f.write(i.get_attribute('outerText') + " EMPTY LINK" + '\n')
                break
            response = requests.get(i.get_attribute('href'))
            st = response.status_code
            f.write(str(st) + ' ' + i.get_attribute('href') + ' ' + i.get_attribute('outerText') + '\n')
        try:
            copyright_text = driver.find_element_by_css_selector('body footer')
            if len(copyright_text.get_attribute('outerText')) > 0:
                f.write("Copyright : " + copyright_text.get_attribute('outerText') + '\n')
        except NoSuchElementException:
            pass
        try:
            copyr_list = driver.find_elements_by_css_selector('body > div > div > span')
            for cop in copyr_list:
                if len(cop.get_attribute('outerText')) > 2:
                    f.write("Copyright : " + cop.get_attribute('outerText') + '\n')
        except NoSuchElementException:
            f.write('No Copyright' + '\n')
        h3_texts = driver.find_elements_by_css_selector('body h3')
        for h in h3_texts:
            h3_list.append(h.get_attribute('outerText'))
        h2_texts = driver.find_elements_by_css_selector('body h2')
        for h in h2_texts:
            h2_list.append(h.get_attribute('outerText'))
        if len(h3_texts) > 0:
            f.write(str([h3.get_attribute('outerText') for h3 in h3_texts])+ '\n')
            b3 = [i for i in h3_list if h3_list.count(i) >= 2]
            if len(b3) > 0:
                f.write(str(b3))
                f.write('\n')
                f.write('Fail' + '\n')
            else:
                f.write('OK - No Repeatable H3/H2 Elements' + '\n')
        elif len(h2_texts) > 0:
            f.write(str([h2.get_attribute('outerText') for h2 in h2_texts]) + '\n')
            b2 = [i for i in h2_list if h2_list.count(i) >= 2]
            if len(b2) > 0:
                f.write(str(b2))
                f.write('\n')
                f.write('Fail' + '\n')
            else:
                f.write('OK - No Repeatable H3/H2 Elements' + '\n')
        else:
            f.write('OK - No Repeatable H3/H2 Elements' + '\n')
    h2_list.clear()
    h3_list.clear()
    headers_list.clear()
    try:
        driver.close()
        driver.switch_to.window(current_window)
    except:
        driver.close()


def site_check():
    global list_with_a, list_with_b, list_c, headers, pathf, text_area
    for site in list_dom:
        result = re.split(r'\.', site)
        site_domenoff = result[0]+ '_' + result[1] + '.txt'
        pathf = path_for_sites_check + '/' + site_domenoff
        index = list_dom.index(site)
        a = r'http://deployer.com/cj_installer/admin/sites/' + list_id[index] + r'/edit'
        driver.get(a)
        localization_button = driver.find_element_by_css_selector('body > div > div > section.content > form > div:nth-child(3) > ul > li:nth-child(5) > a')
        localization_button.click()
        text_area = driver.find_element_by_css_selector('#en_content_value').get_attribute('value')

        templet_gene_button = driver.find_element_by_css_selector('body > div > div > section.content > form > div:nth-child(3) > ul > li:nth-child(4) > a')
        templet_gene_button.click()
        current_window = driver.window_handles[0]

        go_to_test_site_but = driver.find_element_by_css_selector('div.form-group a.btn-primary.btn:nth-child(2)')
        go_to_test_site_but.click()

        next_window = driver.window_handles[1]
        driver.switch_to.window(next_window)
        driver.implicitly_wait(10)

        if len(driver.find_elements_by_css_selector('body nav > div > ul > li'))!=0:
            list_with_a = driver.find_elements_by_css_selector('body nav > div > ul > li a')
            list_with_b = driver.find_elements_by_css_selector('body nav > div > ul > li div a')
            headers = driver.find_elements_by_css_selector('body nav > div > ul > li')
            conditions(index, site, current_window, pathf, text_area)
        elif len(driver.find_elements_by_css_selector('body > header > div > nav > ul > li')) !=0 :
            list_with_a = driver.find_elements_by_css_selector('body > header > div > nav > ul > li a')
            list_with_b = driver.find_elements_by_css_selector('body > header > div > nav > ul > li div a')
            headers = driver.find_elements_by_css_selector('body > header > div > nav > ul > li')
            conditions(index, site, current_window, pathf, text_area)
        elif len(driver.find_elements_by_css_selector('body > header > nav > div > ul > li')) != 0:
            list_with_a = driver.find_elements_by_css_selector('body > header > div > nav > ul > li a')
            list_with_b = driver.find_elements_by_css_selector('body > header > div > nav > ul > li div a')
            headers = driver.find_elements_by_css_selector('body > header > nav > div > ul > li')
            conditions(index, site, current_window, pathf, text_area)
        elif len(driver.find_elements_by_css_selector('body > header > div > div > ul > li')) !=0 :
            list_with_a = driver.find_elements_by_css_selector('body > header > div > div > ul > li a')
            list_with_b = driver.find_elements_by_css_selector('body > header > div > div > ul > li div a')
            headers = driver.find_elements_by_css_selector('body > header > div > div > ul > li')
            conditions(index, site, current_window, pathf, text_area)
        elif len(driver.find_elements_by_css_selector('body > div:nth-child(2) > div > ul > li')) != 0 and len(driver.find_elements_by_css_selector('body > div:nth-child(2) > div > ul > li')) < 15:# length of element > 1
            list_with_a = driver.find_elements_by_css_selector('body > div:nth-child(2) > div > ul > li a')
            list_with_b = driver.find_elements_by_css_selector('body > div:nth-child(2) > div > ul > li div a')
            headers = driver.find_elements_by_css_selector('body > div:nth-child(2) > div > ul > li')
            conditions(index, site, current_window, pathf, text_area)
        elif len(driver.find_elements_by_css_selector('body > div > div > div > ul > li > a')) !=0 :
            list_with_a = driver.find_elements_by_css_selector('body > div > div > div > ul > li > a')
            list_with_b = driver.find_elements_by_css_selector('body > header > div > div > ul > li ul a')
            headers = driver.find_elements_by_css_selector('body > header > div > div > ul > li a')
            conditions(index, site, current_window, pathf, text_area)
        else:
            list_with_a = []
            list_with_b = []
            headers = []
            conditions(index, site, current_window, pathf, text_area)
            print(str(index + 1) + ') ' + site)
            print("No Elements")
    driver.quit()


def main():
    cj_log()
    start = time.localtime()
    #print(start)
    sites_select_pagg()
    site_check()
    end = time.localtime()
    #print(end)
    a = end.tm_min - start.tm_min
    if a < 0:
        a = a + 60
        print(str(a) + ' minutes')
    else:
        print(str(end.tm_min - start.tm_min) + ' minutes')
if __name__ == '__main__':
    main()