from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
import os, re
import urllib.request


list_thumbs = []
path_for_chrome_images = '/Users/testguru/Desktop/path_for_chrome_images' # Путь для сохранения картинок
if not os.path.exists(path_for_chrome_images):
    os.mkdir(path_for_chrome_images)


def image_downloading_chrome():
    keyword = input('Введите кейворд поиска' + '\n')
    driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
    driver.maximize_window()
    driver.get('https://www.google.com')
    search_bar = driver.find_element_by_css_selector("input.gLFyf")
    search_bar.send_keys(keyword+Keys.ENTER)
    time.sleep(1)
    images_button = driver.find_elements_by_css_selector('div.hdtb-mitem a')[0]
    images_button.click()
    show_more = driver.find_element_by_css_selector('input.mye4qd')

    end = driver.find_element_by_css_selector('div.OuJzKb:nth-child(1) div')
    while end.text != 'Looks like you\'ve reached the end':
        try:
            show_more.click()
        except:
            driver.execute_script('window.scrollTo(0,1000000000)')
            time.sleep(1)
    thumbs = driver.find_elements_by_css_selector('img.rg_i.Q4LuWd.tx8vtf')
    keyword_real = re.sub(' ', '_', keyword)
    path = path_for_chrome_images + '/' + keyword_real + str(len(thumbs)) + '/'
    if not os.path.exists(path):
        os.mkdir(path)
    for thumb in thumbs:
        res = thumb.get_attribute('src')
        list_thumbs.append(res)
    for l in list_thumbs:
        a = list_thumbs.index(l)
        if l != None:
            urllib.request.urlretrieve(l, path + str(a) + '.jpg')
    list_thumbs.clear()
    driver.quit()


image_downloading_chrome()
