from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import openpyxl
import re, os, sys
from openpyxl import load_workbook
import time
import quiz

length = []
list_batch = []
task_list_todo = []
digit_list_todo = []
links_list_todo = []

task_list_inprogress = []
digit_list_inprogress = []
links_list_inprogress = []

task_list_done = []
digit_list_done = []
links_list_done = []
list_subtask = []

list_dom = []
list_id = []
list_preset = []
list_column = []
list_row = []
list_domain_keyword = []

jira_dom = []
names_not = []


webpage = input('Введите URL Задачи, пример -> https://jira.89team.online/browse/MS-2161 и нажмите Enter : ' + '\n').rstrip().lstrip()
taska = re.sub(r'https://jira.89team.online/browse/', '', webpage)
driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
# driver.maximize_window()


class Launch_task():

    def __init__(self):
        # webpage = input(
        #     'Введите URL Задачи, пример -> https://jira.89team.online/browse/MS-2161 и нажмите Enter : ' + '\n').rstrip().lstrip()
        # taska = re.sub(r'https://jira.89team.online/browse/', '', webpage)
        # driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
        driver.maximize_window()

    def jira_logging(self):
        driver.get('https://jira.89team.online/secure/RapidBoard.jspa?rapidView=9&view=detail&quickFilter=17')
        jira_login = driver.find_element_by_xpath('//*[@id="login-form-username"]')
        jira_login.send_keys('логин')
        jira_password = driver.find_element_by_xpath('//*[@id="login-form-password"]')
        jira_password.send_keys('пароль')
        jira_submmit = driver.find_element_by_xpath('//*[@id="login-form-submit"]')
        jira_submmit.click()
        driver.implicitly_wait(30)

    def task_select(self):
        sub_tasks = driver.find_elements_by_css_selector('div.ghx-key a')
        for subtask in sub_tasks:
            list_subtask.append(subtask.get_attribute('href'))
        tasks = driver.find_elements_by_css_selector('#ghx-pool > div > ul.ghx-columns li.ghx-column.ui-sortable')
        task_pattern = re.compile(r'^(\w\w-\d+)')
        for task in tasks[::3]:
            task_list_todo.append(str(task.get_attribute('textContent')))
        for sort_task in task_list_todo:
            if sort_task != '':
                s = re.split(task_pattern, sort_task)
                digit_list_todo.append(int(re.sub(r'\D', '', s[1])) - 1)
        for digit in digit_list_todo:
            links_list_todo.append(r'https://jira.89team.online/browse/MS-' + str(digit))

        for task in tasks[1::3]:
            task_list_inprogress.append(str(task.get_attribute('textContent')))
        for sort_task in task_list_inprogress:
            if sort_task != '':
                s = re.split(task_pattern, sort_task)
                digit_list_inprogress.append(int(re.sub(r'\D', '', s[1])) - 1)
        for digit in digit_list_inprogress:
            links_list_inprogress.append(r'https://jira.89team.online/browse/MS-' + str(digit))

        for task in tasks[2::3]:
            task_list_done.append(str(task.get_attribute('textContent')))
        for sort_task in task_list_done:
            if sort_task != '':
                s = re.split(task_pattern, sort_task)
                digit_list_done.append(int(re.sub(r'\D', '', s[1])) - 1)
        for digit in digit_list_done:
            links_list_done.append(r'https://jira.89team.online/browse/MS-' + str(digit))

        while webpage not in links_list_todo:
            if webpage in links_list_done:
                print('Задача ' + taska + " Уже Выполнена")
                quiz.quizz()
            elif webpage in links_list_inprogress:
                while webpage in links_list_inprogress:
                    print('Задача ' + taska + " Уже Выполняется")
                    quiz.quizz()
            elif webpage == '':
                print('Вы ввели пустое поле')
                quiz.quizz()
            elif webpage in list_subtask:
                print('Выбрана Подзадача, Необходимо выбрать Задачу')
                quiz.quizz()
            else:
                print(webpage + ' <-- Такой Задачи Нет В Этом Спринте, Или URL Введен не по формату')
                quiz.quizz()
        print('Вы выбрали Задачу ' + taska)
        print('------------------------------------------------------------------------------------------')



if __name__=='__main__':
    d = Launch_task()
    d.jira_logging()
    d.task_select()
