from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os,glob
import time

chrome_driver_path = '/Users/testguru/Desktop/Excell/chromedriver'
username = 'sdfszdf'
password = 'agsaddg'
base_url = 'localhost:8080/'  # ie 127.0.0.1:8080,
baseUrl = 'http://%s:%s@%s' % (username, password, base_url)
driver = webdriver.Chrome(executable_path=("%s" % chrome_driver_path))
driver.get(baseUrl)
driver.maximize_window()

def main():
    configs_path = '/Users/testguru/Documents/Git/src/main/resources/configs'
    files = os.listdir(configs_path)
    for file_name in files:
        path = os.path.join(configs_path, file_name)
        if os.path.isfile(path) and file_name.endswith('.json'):
            create_tube(file_name, path)

def create_tube(file_name, path):
    with open(path, 'r') as file:
        file_content = file.read()
        create_tube_button = driver.find_element_by_xpath('/html/body/div[2]/nav/div/div/ul/li[1]/a')
        create_tube_button.click()
        crawler_name_field = driver.find_element_by_xpath('//*[@id="name"]')
        crawler_name_field.send_keys(os.path.splitext(file_name)[0]+'.com')
        scrolling_button = driver.find_element_by_xpath('//*[@id="config"]/div/div[1]/div/button')
        scrolling_button.click()
        code_text = driver.find_element_by_xpath('''//*[@id="config"]/div/div[1]/div/div/div/ul/li[4]/button''')
        code_text.click()
        driver.implicitly_wait(10)
        configs_field = driver.find_element_by_xpath('//*[@id="config"]/div/div[2]/textarea')
        configs_field.clear()
        configs_field.send_keys(file_content)
        dumps_path = '/Users/testguru/Documents/Git/src/main/resources/configs/dump'
        path_d = os.path.join(dumps_path, file_name)
        if os.path.isfile(path_d):
            with open(path_d, 'r') as dump:
                dump_content = dump.read()
                dumps_scrolling_button = driver.find_element_by_xpath('//*[@id="dump"]/div/div[1]/div/button')
                dumps_scrolling_button.click()
                dumps_code_text = driver.find_element_by_xpath('//*[@id="dump"]/div/div[1]/div/div/div/ul/li[4]/button')
                dumps_code_text.click()
                dumps_configs_field = driver.find_element_by_xpath('//*[@id="dump"]/div/div[2]/textarea')
                dumps_configs_field.clear()
                dumps_configs_field.send_keys(dump_content)
                submit_button = driver.find_element_by_xpath('/html/body/div[2]/div/form/div[6]/button')
                submit_button.click()
                driver.implicitly_wait(5)
                back_to_crawlers_button = driver.find_element_by_xpath('/html/body/div[2]/nav/div/div/a')
                back_to_crawlers_button.click()
        else:
            submit_button = driver.find_element_by_xpath('/html/body/div[2]/div/form/div[6]/button')
            submit_button.click()
            driver.implicitly_wait(5)
            back_to_crawlers_button = driver.find_element_by_xpath('/html/body/div[2]/nav/div/div/a')
            back_to_crawlers_button.click()

main()
driver.refresh()
time.sleep(3)
sorting_button_name = driver.find_element_by_xpath('//*[@id="tubesTable"]/thead/tr/th[2]')
sorting_button_name.click()
time.sleep(3)
driver.close()
driver.quit()
