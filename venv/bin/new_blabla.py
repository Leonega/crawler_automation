from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import openpyxl
import re, os, sys
from openpyxl import load_workbook
import time



chrome_driver_path = '/Users/testguru/Desktop/Excell/chromedriver'

length = []
list_batch = []
task_list_todo = []
digit_list_todo = []
links_list_todo = []

task_list_inprogress = []
digit_list_inprogress = []
links_list_inprogress = []

task_list_done = []
digit_list_done = []
links_list_done = []
list_subtask = []
serv_list=[]

list_dom = []
list_id = []
list_preset = []
list_column = []
list_row = []
list_domain_keyword = []

jira_dom = []
names_not = []

webpage = input('Введите URL Задачи, пример -> https://jira.89team.online/browse/MS-2161 и нажмите Enter : ' + '\n').rstrip().lstrip()
taska = re.sub(r'https://jira.89team.online/browse/', '', webpage)
driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
action = ActionChains(driver)

def quiz():
    global webpage,taska
    print('Повторить Ввод Заново? y/n')
    answ = input()
    if answ == 'y':
        webpage = input('Введите URL Задачи, пример -> https://jira.89team.online/browse/MS-2161 и нажмите Enter : ' + '\n').rstrip().lstrip()
        taska = re.sub(r'https://jira.89team.online/browse/', '', webpage)
    elif answ == 'n':
        print('Запустите программу сначала!')
        driver.quit()
        sys.exit()
    else:
        print('Вы ввели не "y" и не "n"')
        print('Запустите программу сначала!')
        driver.quit()
        sys.exit()

def jira_logging():
        # driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
        # driver = webdriver.Chrome(executable_path=("%s" % chrome_driver_path))
        # driver.maximize_window()
        driver.get('https://jira.89team.online/secure/RapidBoard.jspa?rapidView=9&view=detail&quickFilter=17')
        jira_login = driver.find_element_by_xpath('//*[@id="login-form-username"]')
        jira_login.send_keys('логин')
                        # print('Введеите Логин в Jira, нажмите Enter ')
                        # login_jira_input = input('')
                        # jira_login.send_keys(login_jira_input)
        jira_password = driver.find_element_by_xpath('//*[@id="login-form-password"]')
        jira_password.send_keys('пароль')
                        # print('Введеите Пароль в Jira, нажмите Enter ')
                        # password_jira_input = input('')
                        # jira_password.send_keys(password_jira_input)
        jira_submmit = driver.find_element_by_xpath('//*[@id="login-form-submit"]')
        jira_submmit.click()
        driver.implicitly_wait(30)

def task_select():
        global webpage, taska
        sub_tasks = driver.find_elements_by_css_selector('div.ghx-key a')
        for subtask in sub_tasks:
            list_subtask.append(subtask.get_attribute('href'))

        try:
            ## Для Tasks
            tasks = driver.find_elements_by_css_selector('#ghx-pool > div > ul.ghx-columns li.ghx-column.ui-sortable')

        except:
            ## Для Subtasks
            tasks = driver.find_elements_by_css_selector('#ghx-pool > div > ul.ghx-columns li.ghx-column.ui-sortable div.ghx-key a')
        task_pattern = re.compile(r'^(\w\w-\d+)')

        for task in tasks[::3]:
            task_list_todo.append(str(task.get_attribute('textContent')))
        print(task_list_todo)
        for sort_task in task_list_todo:
            if sort_task != '':
                s = re.split(task_pattern, sort_task)
                try:
                    ## Для Tasks
                    digit_list_todo.append(int(re.sub(r'\D', '', s[1])) - 1)
                except:
                    ## Для Subtasks
                    digit_list_todo.append(int(re.sub(r'\D', '', s[1])))
        print(digit_list_todo)
        for digit in digit_list_todo:
            links_list_todo.append(r'https://jira.89team.online/browse/MS-' + str(digit))

        for task in tasks[1::3]:
            task_list_inprogress.append(str(task.get_attribute('textContent')))
        for sort_task in task_list_inprogress:
            if sort_task != '':
                s = re.split(task_pattern, sort_task)
                digit_list_inprogress.append(int(re.sub(r'\D', '', s[1])) - 1)
        for digit in digit_list_inprogress:
            links_list_inprogress.append(r'https://jira.89team.online/browse/MS-' + str(digit))

        for task in tasks[2::3]:
            task_list_done.append(str(task.get_attribute('textContent')))
        for sort_task in task_list_done:
            if sort_task != '':
                s = re.split(task_pattern, sort_task)
                digit_list_done.append(int(re.sub(r'\D', '', s[1])) - 1)
        for digit in digit_list_done:
            links_list_done.append(r'https://jira.89team.online/browse/MS-' + str(digit))


        while webpage not in links_list_todo:
            if webpage in links_list_done:
                print('Задача ' + taska + " Уже Выполнена")
                quiz()
            elif webpage in links_list_inprogress:
                while webpage in links_list_inprogress:
                    print('Задача ' + taska + " Уже Выполняется")
                    quiz()
            elif webpage == '':
                print('Вы ввели пустое поле')
                quiz()
            elif webpage in list_subtask:
                print('Выбрана Подзадача, Необходимо выбрать Задачу')
                quiz()
            else:
                print(webpage + ' <-- Такой Задачи Нет В Этом Спринте, Или URL Введен не по формату')
                quiz()
        print('Вы выбрали Задачу ' + taska)
        print('------------------------------------------------------------------------------------------')
        task_data_collect(webpage)


def task_data_collect(webpage):
        driver.get(webpage)
        preset_field = driver.find_element_by_css_selector('#description-val > div > p:nth-child(1)').get_attribute('textContent')
        result = re.split(r'\n', preset_field )
        # print(result)
        preset = driver.find_element_by_xpath('//*[@id="summary-val"]').text
        # print(preset)
        server = re.sub(r'Server: ', '', result[1])
        # print(server)
        ftt_serv =re.sub(r'Ftt Server: Tube ', '', result[2])
        ftt_server = re.findall(r'[(].+[)]', ftt_serv)
        cdn_thumb_serv = re.sub(r'CDN Thumb Server: ', '', result[3])
        if r'-' not in result[3]:                   # cdn_thumb_server = re.findall(r'-\s.*', cdn_thumb_serv)  # Возможен косяк с " - crawler "
            cdn_thumb_serv = re.sub(r'CDN Thumb Server: ', '- ', result[3])
        cdn_thumb_server = re.findall(r'-\s.*', cdn_thumb_serv)
        feed_main_cat = re.sub(r'feed_main_category_id: ', '', result[5])
        batch = driver.find_elements_by_css_selector('#description-val > div > p')
        # if len(batch) > 9:
        #     for p in batch[4:]: # Заменить на " 4 ", если будет доавлена запись <redirect_links/> / после ссылок
        #         list_batch.append(p.text)
        #     print(len(list_batch))
        # else:
        #     for p in batch[3:]: # Заменить на " 4 ", если будет доавлена запись <redirect_links/> / после ссылок
        #         list_batch.append(p.text)
        #     print(len(list_batch))
        for p in batch[4:]:  # Заменить на " 4 ", если будет доавлена запись <redirect_links/> / после ссылок
            list_batch.append(p.text)
        print(len(list_batch))
        excel_table = driver.find_element_by_css_selector('#description-val > div > p a')
        e = excel_table.text
        excel_table.click()
        driver.implicitly_wait(5)
        excel_file_save(e)
        cj_installer_loggin(preset, feed_main_cat, server, ftt_server, cdn_thumb_server)


def excel_file_save(e):   # возможен отказ в доступе на скачивание файла
        driver.close()
        next_window = driver.window_handles[0]
        driver.switch_to.window(next_window)
        if driver.title != 'Учетные записи Zoho' and driver.title != 'Zoho Accounts':
            if len(driver.find_elements_by_css_selector('#filetw > a'))>0:
                    save_to_my = driver.find_element_by_css_selector('#filetw > a')
                    save_to_my.click()
                    time.sleep(2)
                    download_how = driver.find_element_by_css_selector('#exportfileas > span.ui-zmenu-text')
                    download_how.click()
                    time.sleep(2)
                    # driver.implicitly_wait(5)
                    save_format = driver.find_element_by_css_selector('#exportasxlsx.ui-zmenu-item')
                    save_format.click()
                    time.sleep(5)
                    # driver.implicitly_wait(3)
            else:
                save_to_my = driver.find_element_by_css_selector('#TB_FILE_IDENTITY > span')
                save_to_my.click()
                action.send_keys(Keys.DOWN)
                time.sleep(2)
                action.send_keys(Keys.DOWN)
                time.sleep(2)
                action.send_keys(Keys.RIGHT)
                time.sleep(2)
                action.send_keys(Keys.ENTER)
                action.perform()
                driver.implicitly_wait(5)
                time.sleep(5)
        else:
            try:
                sign_in = driver.find_element_by_css_selector('#openidcontainer > div.fs_signin_options_txt')
                sign_in.click()
                driver.implicitly_wait(3)
                google = driver.find_element_by_css_selector('#fs-google-icon')
                google.click()
                input_log = driver.find_element_by_css_selector('#identifierId')
                input_log.send_keys(r'емейл')
                next_but = driver.find_element_by_css_selector('#identifierNext > span > span')
                next_but.click()
                input_pass = driver.find_element_by_css_selector('#password > div.aCsJod.oJeWuf > div > div.Xb9hP > input')
                input_pass.send_keys('пароль' + Keys.ENTER)
                driver.implicitly_wait(3)
                save_to_my = driver.find_element_by_css_selector('#filetw > a')
                save_to_my.click()
                download_how = driver.find_element_by_css_selector('#exportfileas > span.ui-zmenu-text')
                download_how.click()
                driver.implicitly_wait(5)
                save_format = driver.find_element_by_css_selector('#exportasxlsx.ui-zmenu-item')
                save_format.click()
                driver.implicitly_wait(3)
                time.sleep(2)
            except:
                enter_zoho = driver.find_element_by_css_selector('#MI_242_IDENTITY')
                enter_zoho.click()
                zoho_login = driver.find_element_by_css_selector('#block-system-main > div > div.header-part > span > a')
                zoho_login.click()
                sign_in = driver.find_element_by_css_selector('#openidcontainer > div.fs_signin_options_txt')
                sign_in.click()
                driver.implicitly_wait(3)
                google = driver.find_element_by_css_selector('#fs-google-icon')
                google.click()
                input_log = driver.find_element_by_css_selector('#identifierId')
                input_log.send_keys(r'емейл')
                next_but = driver.find_element_by_css_selector('#identifierNext > span > span')
                next_but.click()
                input_pass = driver.find_element_by_css_selector('#password > div.aCsJod.oJeWuf > div > div.Xb9hP > input')
                input_pass.send_keys('пароль' + Keys.ENTER)
                driver.implicitly_wait(3)
                driver.get(e)
                save_to_my = driver.find_element_by_css_selector('#TB_FILE_IDENTITY > span')
                save_to_my.click()
                download_how = driver.find_element_by_css_selector('#MI_8_MENU > span')
                download_how.click()
                driver.implicitly_wait(5)
                save_format = driver.find_element_by_css_selector('#SMI_8_IDENTITY > li:nth-child(1)')
                save_format.click()
                driver.implicitly_wait(3)
                time.sleep(2)


def cj_installer_loggin(preset,feed_main_cat,server,ftt_server,cdn_thumb_server):
        driver.get('http://deployer.com/cj_installer/admin/login')
        driver.maximize_window()
        driver.implicitly_wait(10)
        cj_login_input = driver.find_element_by_xpath('//*[@id="username"]')
        cj_login_input.send_keys('логин')
        cj_password_input = driver.find_element_by_xpath('//*[@id="password"]')
        cj_password_input.send_keys('пароль')
        cj_press_button = driver.find_element_by_xpath('/html/body/div/div[2]/form/div[3]/div[2]/button')
        cj_press_button.click()
        cj_data_input_preset(preset,feed_main_cat,server,ftt_server,cdn_thumb_server)

def cj_data_input_preset(preset, feed_main_cat, server, ftt_server, cdn_thumb_server):
        for n in range(len(list_batch)+1):
                driver.get('http://deployer.com/cj_installer/admin/login')
                sites_button = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/a')
                sites_button.click()
                add_batch_sites = driver.find_element_by_xpath('/html/body/div/aside/section/ul/li[1]/ul/li[4]/a')
                add_batch_sites.click()

                preset_field = driver.find_element_by_xpath('//*[@id="select2-preset_id-container"]')
                preset_field.click()
                preset_input = driver.find_element_by_xpath('/html/body/span/span/span[1]/input')
                preset_input.send_keys(preset + Keys.ENTER)

                server_button = driver.find_elements_by_css_selector('#server_id option')
                for server_cj in server_button:
                    # print(server_cj.text,type(server_cj.text),server_cj.get_attribute('label'))
                    # print(server, type(server))
                    if server_cj.text == server.lstrip().rstrip(): # server был с пробелом ' ko32-roman'
                        server_cj.click()

                ftt_server_button = driver.find_elements_by_css_selector('#ftt_server_id option')
                for ftt_server_cj in ftt_server_button:
                    if ftt_server[0] in ftt_server_cj.text:
                        ftt_server_cj.click()

                cdn_thumb_button = driver.find_elements_by_css_selector('#cdn_thumb_server_id option')
                for cdn_thumb_cj in cdn_thumb_button:
                    if cdn_thumb_server[0] in cdn_thumb_cj.text:
                        cdn_thumb_cj.click()

                for text in list_batch[n:]:
                    text_area = driver.find_element_by_css_selector('#batch')
                    text_area.send_keys(text + '\n\n')
                if n == len(list_batch):
                        print(r'Сайты\Сайт Из Задачи ' + taska + r' Уже Были Добалены Ранее Или Задача Не Переведена Из "To-DO" --> "In Progress", Проверить Входные\Выходные Данные')
                        work_excell_file()
                        driver.quit()
                        sys.exit()
                add_batch_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/section/form/div/div[8]/div/input')
                add_batch_button.click()
                time.sleep(3)
                if driver.current_url == 'http://deployer.com/cj_installer/admin/sites/batch' and n < len(list_batch):
                    n += 1
                else:
                    break 

        pagging = driver.find_elements_by_css_selector('#DataTables_Table_0_paginate > ul li a')
        pag = int(pagging[-2].text) # номер последней страницы паггинации
        for lb in list_batch:
            jd = re.findall('.+\.\D+(?=\\n)',lb)
            for j in jd:
                jira_dom.append(j.lower())
        paggination(pag)
        work_excell_file()
        input_keywords(feed_main_cat, preset)

def work_excell_file():
        filenames = os.listdir('/Users/testguru/Downloads')
        for f in filenames:
            if f.startswith('domains') or f.startswith('Копия') or f.startswith('Copy of domains') or f.startswith('Domains') or f.startswith('door') or f.startswith('doorways') or f.startswith('18022020') and f.endswith('.xlsx'):
                wd = '/Users/testguru/Downloads/' + f
                wb = load_workbook(wd)
                # sheet = wb['Лист1']
                sheet = wb.active
                for row in sheet['A1':'A80']:
                    for cell in row:
                        list_column.append(str(cell.value).lower())
                for row in sheet['E1':'E80']: ### Изначально было ['E1':'E60']
                    for cell in row:
                        list_row.append(str(cell.value).lower())
                for el in range(len(list_column)):
                    list_domain_keyword.append(str(list_column[el]) + " " + str(list_row[el]))
                os.remove(wd)

def paggination(pag):
        for page in range(1,pag+1):
            collect_domain_name = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(2)')
            collect_id_number = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(1)')
            current_preset = driver.find_elements_by_css_selector('#DataTables_Table_0 > tbody > tr > td:nth-child(3)')
            for dom in collect_domain_name:
                list_dom.append(dom.text)
            for num in collect_id_number:
                list_id.append(num.text)
            for cur in current_preset:
                list_preset.append(cur.text)
            page_button = driver.find_element_by_css_selector('#DataTables_Table_0_next > a')
            page_button.click()
            driver.implicitly_wait(10)
            time.sleep(4)


def input_keywords(feed_main_cat,preset):
        for pr in range(len(list_preset)):
            if preset == list_preset[pr]: # пресеты могут совпадать, тогда вручную
                for site in list_dom:
                            driver.implicitly_wait(20)
                            if site.lower() == list_dom[pr] and site.lower() in jira_dom:
                                index = list_dom.index(site)
                                column_index = list_column.index(site)
                                print(site + ' ID - ' + list_id[index] + ' KEYWORDS - ' + str(list_row[column_index]))
                                a = r'http://deployer.com/cj_installer/admin/sites/' + list_id[index] + r'/edit'
                                driver.get(a)
                                private_sett_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/form/div[1]/ul/li[2]/a')
                                private_sett_button.click()
                                feed_main_cat_id_field = driver.find_element_by_css_selector('#feed_main_category_id')
                                search_base_name = driver.find_element_by_xpath('//*[@id="search_base_name"]')
                                if str(list_row[column_index]) == 'none':
                                    search_base_name.clear()
                                else:
                                    search_base_name.clear()
                                    search_base_name.send_keys(str(list_row[column_index]))
                                if feed_main_cat != 0:
                                    feed_main_cat_id_field.clear()
                                    feed_main_cat_id_field.send_keys(feed_main_cat)
                                save_button = driver.find_element_by_xpath('/html/body/div/div/section[2]/form/div[2]/input')
                                save_button.click()
                                length.append(site)
        names = ''
        for numb in range(len(length)):
            names = names + ' ' + str(length[numb])
        bla = names.lstrip().rstrip()
        bl = re.sub(' ',', ',bla)

        if 'Sites' in driver.title:
            print('------------------------------------------------------------------------------------------')
            print('Пачка из ' + str(len(length))  + ' сайтов успешно добавлена,' + ' - ' + bl)

        for sitedom in jira_dom:
            if sitedom not in list_dom:
                names_not.append(sitedom)

        nam =''
        for q in range(len(names_not)):
            nam = nam + ' ' + str(names_not[q])
        end = nam.lstrip().rstrip()
        en = re.sub(' ',', ',end)
        if en != '':
            print("Сайт\Сайты " + en + " Не добавлен\ны!")
        driver.quit()


def main():
    driver.maximize_window()
    jira_logging()
    task_select()

if __name__ == '__main__':
    main()



